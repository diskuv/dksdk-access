#!/bin/sh
set -euf

# usage:
#   sh test-posix-shell.sh TEST_NAME [-x]
# examples:
#   sh test-posix-shell.sh test_no_args
#   sh test-posix-shell.sh test_1_0 -x

TEST_NAME=$1
shift

if [ $# -gt 0 ] && [ "$1" = -x ]; then
    set -x
fi

# shellcheck disable=SC1091
. @PROJECT_SOURCE_DIR@/shells/access.source.sh

test_no_args() {
    dksdk_access
}

test_bad_REQUEST_DKSDK_VERSION() {
    dksdk_access xyz
}

test_REQUEST_DKSDK_VERSION_1_0() {
    dksdk_access 1.0
    set | grep ^dksdk_access_ # print variables
}

# Run the test
eval "$TEST_NAME"
