configure_file(test-posix-shell.in.sh test-posix-shell.sh
    FILE_PERMISSIONS OWNER_READ OWNER_WRITE OWNER_EXECUTE
    @ONLY NEWLINE_STYLE UNIX)

add_test(NAME posix-shell-fail-no-args COMMAND test-posix-shell.sh test_no_args)
set_tests_properties(posix-shell-fail-no-args PROPERTIES LABELS "Unix" PASS_REGULAR_EXPRESSION [[usage: dksdk_access]])


add_test(NAME posix-shell-bad-REQUEST_DKSDK_VERSION COMMAND test-posix-shell.sh test_bad_REQUEST_DKSDK_VERSION)
set_tests_properties(posix-shell-bad-REQUEST_DKSDK_VERSION PROPERTIES LABELS "Unix" PASS_REGULAR_EXPRESSION [[not a valid version]])

add_test(NAME posix-shell-REQUEST_DKSDK_VERSION-1-0 COMMAND test-posix-shell.sh test_REQUEST_DKSDK_VERSION_1_0)
set_tests_properties(posix-shell-REQUEST_DKSDK_VERSION-1-0 PROPERTIES LABELS "Unix"
    PASS_REGULAR_EXPRESSION [[dksdk_access_DKSDK_DATA_HOME=.*dksdk_access_REQUEST_DKSDK_VERSION_MAJOR='1'.*dksdk_access_REQUEST_DKSDK_VERSION_MINOR='0']])
