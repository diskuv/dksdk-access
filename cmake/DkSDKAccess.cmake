# The purpose of this module is to let the DkSDK user
# configure the location of DkSDK, including any authentication
# if it is not local.
#
# The input is the CMake variable `REQUEST_DKSDK_VERSION`
#
# Other optional inputs are:
# `DKSDK_DATA_HOME`
# `dksdk-BASE_REPOSITORY_URL`
# `DKSDK_DISABLE_AUTOACCESS_DKSDK_CMAKE` - for private use of dksdk-cmake project
#
# The output are the CMake variables:
# `DKSDK_DATA_HOME`
# `dksdk-BASE_REPOSITORY_URL`
# `dksdk-cmake_SOURCE_DIR`

# --- Parse REQUEST_DKSDK_VERSION
if(NOT REQUEST_DKSDK_VERSION)
    message(FATAL_ERROR [[The CMake variable REQUEST_DKSDK_VERSION was not set.

REQUEST_DKSDK_VERSION should be set to the version of DkSDK you want
your project to use. The format is MAJOR.MINOR. An example would be:

set(REQUEST_DKSDK_VERSION 1.0)
]])
endif()

if(NOT REQUEST_DKSDK_VERSION MATCHES "^[0-9]+[.][0-9]+$")
    message(FATAL_ERROR [[The CMake variable REQUEST_DKSDK_VERSION is not a valid version.

REQUEST_DKSDK_VERSION should be set to the version of DkSDK you want
your project to use. The format is MAJOR.MINOR. An example would be:

set(REQUEST_DKSDK_VERSION 1.0)
]])
endif()

string(REPLACE "." ";" REQUEST_DKSDK_VERSION_terms ${REQUEST_DKSDK_VERSION})
list(GET REQUEST_DKSDK_VERSION_terms 0 REQUEST_DKSDK_VERSION_MAJOR)
list(GET REQUEST_DKSDK_VERSION_terms 1 REQUEST_DKSDK_VERSION_MINOR)

# Ex. REQUEST_DKSDK_VERSION_underscore=1_0
set(REQUEST_DKSDK_VERSION_underscore "${REQUEST_DKSDK_VERSION_MAJOR}_${REQUEST_DKSDK_VERSION_MINOR}")

# Track whether we should warn the user that DkSDK CMake (and DkSDK in general) has not been configured
set(_dksdk-cmake_USER_CONFIGURED_SOMETHING OFF)

# --- Locate DKSDK_DATA_HOME
if(DKSDK_DATA_HOME) # ex. cmake -D DKSDK_DATA_HOME=...
    message(STATUS "Using DKSDK_DATA_HOME CMake variable: ${DKSDK_DATA_HOME}")
elseif(DEFINED ENV{XDG_DATA_HOME}) # ie. modern Unix or Windows CI
    set(DKSDK_DATA_HOME "$ENV{XDG_DATA_HOME}/dksdk")
    message(STATUS "Using <XDG_DATA_HOME>/dksdk as DKSDK_DATA_HOME: ${DKSDK_DATA_HOME}")
elseif(CMAKE_HOST_WIN32 AND DEFINED ENV{LOCALAPPDATA}) # ie. Windows
    set(DKSDK_DATA_HOME "$ENV{LOCALAPPDATA}/DkSDK")
    cmake_path(NORMAL_PATH DKSDK_DATA_HOME)
    message(STATUS "Using <LOCALAPPDATA>/DkSDK as DKSDK_DATA_HOME: ${DKSDK_DATA_HOME}")
elseif(DEFINED ENV{HOME}) # ie. legacy Unix
    set(DKSDK_DATA_HOME "$ENV{HOME}/.local/share/dksdk")
    message(STATUS "Using <HOME>/.local/share/dksdk as DKSDK_DATA_HOME: ${DKSDK_DATA_HOME}")
endif()

# --- Set dksdk-BASE_REPOSITORY_URL

if(dksdk-BASE_REPOSITORY_URL)
    # the user set a configuration parameter
    set(_dksdk-cmake_USER_CONFIGURED_SOMETHING ON)
elseif(NOT dksdk-BASE_REPOSITORY_URL AND EXISTS "${DKSDK_DATA_HOME}/repository.ini")
    # override repository with file contents (prepend comment lines with a # symbol)
    cmake_policy(PUSH)
    if(POLICY CMP0159)
        cmake_policy(SET CMP0159 NEW) # file(STRINGS) with REGEX updates CMAKE_MATCH_<n>
    endif()
    file(STRINGS "${DKSDK_DATA_HOME}/repository.ini" repository_line
        LIMIT_COUNT 1 LIMIT_INPUT 8192 NO_HEX_CONVERSION REGEX "^${REQUEST_DKSDK_VERSION_underscore}[ \t]*=[ \t]*(https://|git@)")
    cmake_policy(POP)

    # 1_0 = https://x/y/z.git ==> https://x/y/z.git
    string(REGEX REPLACE "^${REQUEST_DKSDK_VERSION_underscore}[ \t]*=[ \t]*" ""
        dksdk-BASE_REPOSITORY_URL "${repository_line}")
    string(STRIP "${dksdk-BASE_REPOSITORY_URL}" dksdk-BASE_REPOSITORY_URL) # No CR (0x0D) or trailing spaces
    unset(repository_line)

    # the user set a configuration parameter
    set(_dksdk-cmake_USER_CONFIGURED_SOMETHING ON)

    if(dksdk-BASE_REPOSITORY_URL)
        # potential sensitive user data
        message(STATUS "Using DkSDK repositories based on the ${REQUEST_DKSDK_VERSION_underscore} = <redacted> repository setting in ${DKSDK_DATA_HOME}/repository.ini")
    else()
        message(WARNING "There was no ${REQUEST_DKSDK_VERSION_underscore} = <redacted> repository setting in ${DKSDK_DATA_HOME}/repository.ini. Trying fallbacks ...")
    endif()
endif()

if(NOT dksdk-BASE_REPOSITORY_URL) # default
    if(DEFINED ENV{DKSDK_BASE_REPO_${REQUEST_DKSDK_VERSION_underscore}}) # override base of repository with environment variable
        # potential sensitive user data
        set(dksdk-BASE_REPOSITORY_URL "$ENV{DKSDK_BASE_REPO_${REQUEST_DKSDK_VERSION_underscore}}")
        message(STATUS "Using DkSDK repositories based on the DKSDK_BASE_REPO_${REQUEST_DKSDK_VERSION_underscore} environment variable")

        # the user set a configuration parameter
        set(_dksdk-cmake_USER_CONFIGURED_SOMETHING ON)
    else()
        set(dksdk-BASE_REPOSITORY_URL https://gitlab.com/diskuv/distributions/${REQUEST_DKSDK_VERSION})
        message(STATUS "Using default ${dksdk-BASE_REPOSITORY_URL} DkSDK repositories")
    endif()
endif()

# --- Set dksdk-cmake_REPOSITORY

macro(configure_repository0 SUBPATH NAME NAME_UPPER_UNDERSCORE DESCR)
    # Example:
    # if(DEFINED ENV{DKSDK_CMAKE_REPO_${REQUEST_DKSDK_VERSION_underscore}})
    #     set(${NAME}_REPOSITORY "$ENV{DKSDK_CMAKE_REPO_${REQUEST_DKSDK_VERSION_underscore}}")
    #     message(STATUS "Using DkSDK CMake repository from the DKSDK_CMAKE_REPO_${REQUEST_DKSDK_VERSION_underscore} environment variable")
    # elseif(NOT ${NAME}_REPOSITORY)
    #     set(${NAME}_REPOSITORY "${dksdk-BASE_REPOSITORY_URL}/${NAME}.git")
    # endif()
    if(DEFINED ENV{${NAME_UPPER_UNDERSCORE}_REPO_${REQUEST_DKSDK_VERSION_underscore}}) # override repository with environment variable
        # potential sensitive user data
        set(${NAME}_REPOSITORY "$ENV{${NAME_UPPER_UNDERSCORE}_REPO_${REQUEST_DKSDK_VERSION_underscore}}")
        message(STATUS "Using ${DESCR} repository from the ${NAME_UPPER_UNDERSCORE}_REPO_${REQUEST_DKSDK_VERSION_underscore} environment variable")

        # the user set a configuration parameter
        if("${NAME}" STREQUAL dksdk-cmake)
            set(_dksdk-cmake_USER_CONFIGURED_SOMETHING ON)
        endif()
    elseif(DEFINED ENV{${NAME_UPPER_UNDERSCORE}_BRANCH_${REQUEST_DKSDK_VERSION_underscore}}) # override branch with environment variable
        # potential sensitive user data
        set(${NAME}_REPOSITORY "${dksdk-BASE_REPOSITORY_URL}/${SUBPATH}.git#$ENV{${NAME_UPPER_UNDERSCORE}_BRANCH_${REQUEST_DKSDK_VERSION_underscore}}")
        message(STATUS "Using default ${DESCR} repository with the $ENV{${NAME_UPPER_UNDERSCORE}_BRANCH_${REQUEST_DKSDK_VERSION_underscore}} branch")

        # the user set a configuration parameter
        if("${NAME}" STREQUAL dksdk-cmake)
            set(_dksdk-cmake_USER_CONFIGURED_SOMETHING ON)
        endif()
    elseif(NOT ${NAME}_REPOSITORY)
        set(${NAME}_REPOSITORY "${dksdk-BASE_REPOSITORY_URL}/${SUBPATH}.git")
    endif()
endmacro()
macro(configure_repository NAME NAME_UPPER_UNDERSCORE DESCR)
    configure_repository0(${NAME} ${NAME} ${NAME_UPPER_UNDERSCORE} "${DESCR}")
endmacro()
macro(configure_repository_port NAME NAME_UPPER_UNDERSCORE DESCR)
    configure_repository0(ports/${NAME} dksdk-port-${NAME} DKSDK_PORT_${NAME_UPPER_UNDERSCORE} "${DESCR}")
endmacro()


configure_repository(dksdk-cmake                DKSDK_CMAKE         "DkSDK CMake" "")
configure_repository(dksdk-coder                DKSDK_CODER         "DkSDK Coder" "")
configure_repository(dksdk-ffi-c                DKSDK_FFI_C         "DkSDK FFI C" "")
configure_repository(dksdk-ffi-java             DKSDK_FFI_JAVA      "DkSDK FFI Java" "")
configure_repository(dksdk-ffi-ocaml            DKSDK_FFI_OCAML     "DkSDK FFI OCaml" "")
configure_repository(dksdk-opam-repository-core DKSDK_OPAM_CORE     "DkSDK Opam Core Repository" "")
configure_repository(dksdk-opam-repository-js   DKSDK_OPAM_JS       "DkSDK Opam JavaScript Repository" "")
configure_repository_port(ocaml-ctypes          OCAML_CTYPES        "DkSDK port of ocaml-ctypes")

# --- Add token

# Group or project access token, not OAuth2 token, so do not use "oauth2" as the username.
# Instead use "gitlab-ci-token" which is required to clone a private project. Confer:
# - https://docs.gitlab.com/ee/api/oauth2.html#access-git-over-https-with-access-token
# - https://docs.gitlab.com/ee/user/group/settings/group_access_tokens.html
# - https://docs.gitlab.com/ee/ci/jobs/ci_job_token.html
set(_dksdk-access_CREDS)
if(DEFINED ENV{DKSDK_TOKEN_${REQUEST_DKSDK_VERSION_underscore}_DISABLE} AND "$ENV{DKSDK_TOKEN_${REQUEST_DKSDK_VERSION_underscore}_DISABLE}" STREQUAL "true")
    message(STATUS "Disabling checks for a DkSDK token and a GitLab CI token environment variables because of `true` DKSDK_TOKEN_${REQUEST_DKSDK_VERSION_underscore} environment variable")
elseif(DEFINED ENV{DKSDK_TOKEN_${REQUEST_DKSDK_VERSION_underscore}}) # takes precedence
    set(_dksdk-access_CREDS "dksdk-access:$ENV{DKSDK_TOKEN_${REQUEST_DKSDK_VERSION_underscore}}")
    message(STATUS "Found dksdk-access token from DKSDK_TOKEN_${REQUEST_DKSDK_VERSION_underscore} environment variable")
elseif(DEFINED ENV{CI_JOB_TOKEN} AND DEFINED ENV{GITLAB_CI} AND "$ENV{GITLAB_CI}" STREQUAL "true")
    # Use "gitlab-ci-token" which is required to clone a private project from within GitLab CI.
    # Confer: https://docs.gitlab.com/ee/ci/jobs/ci_job_token.html
    # https://docs.gitlab.com/ee/ci/variables/predefined_variables.html
    set(_dksdk-access_CREDS "gitlab-ci-token:$ENV{CI_JOB_TOKEN}")
    message(STATUS "Found gitlab-ci-token token from CI_JOB_TOKEN environment variable")
endif()

# Do not add environment credentials if already present in repository.ini or in the supplied URLs.
# That is, only accept one set of credentials in URL.
macro(add_creds_if_none VAR)
    if(NOT "${${VAR}}" MATCHES "^https://[^/]+@")
        string(REGEX REPLACE "^https://" "https://${_dksdk-access_CREDS}@" ${VAR} "${${VAR}}")
    endif()
endmacro()

if(_dksdk-access_CREDS)
    # the user set a configuration parameter
    set(_dksdk-cmake_USER_CONFIGURED_SOMETHING ON)

    # potential sensitive user data

    add_creds_if_none(dksdk-BASE_REPOSITORY_URL)
    add_creds_if_none(dksdk-cmake_REPOSITORY)
    add_creds_if_none(dksdk-coder_REPOSITORY)
    add_creds_if_none(dksdk-ffi-c_REPOSITORY)
    add_creds_if_none(dksdk-ffi-java_REPOSITORY)
    add_creds_if_none(dksdk-ffi-ocaml_REPOSITORY)
    add_creds_if_none(dksdk-opam-repository-core_REPOSITORY)
    add_creds_if_none(dksdk-opam-repository-js_REPOSITORY)
    add_creds_if_none(dksdk-port-ocaml-ctypes_REPOSITORY)
    unset(_dksdk-access_CREDS)
endif()

# Stop leaky variables
unset(REQUEST_DKSDK_VERSION_terms)
unset(REQUEST_DKSDK_VERSION_underscore)

# Common details to fetch dksdk-cmake
#   Mimic `#` split like DkSDKQuerySource.cmake:_DkSDKQuerySource_ParseAndStoreValidUrl
set(_dksdk_cmake_GITREF origin/main)
if("${dksdk-cmake_REPOSITORY}" MATCHES "^(https://[^#]*[.]git)($|#.*)")
    set(_dksdk_cmake_GITREPOSITORY "${CMAKE_MATCH_1}")
    if(CMAKE_MATCH_COUNT EQUAL 2)
        set(_dksdk_cmake_GITREF "${CMAKE_MATCH_2}")
        string(REGEX REPLACE "^#" "" _dksdk_cmake_GITREF "${_dksdk_cmake_GITREF}")
    endif()
else()
    set(_dksdk_cmake_GITREPOSITORY "${dksdk-cmake_REPOSITORY}")
endif()

if(CMAKE_SCRIPT_MODE_FILE AND FETCH_DKSDK_CMAKE)
    include(FetchContent)

    # --- Fetch dksdk-cmake
    cmake_path(APPEND CMAKE_CURRENT_BINARY_DIR "dksdk-cmake-src" OUTPUT_VARIABLE dksdk-cmake_SOURCE_DIR)
    FetchContent_Populate(dksdk-cmake
        QUIET
        SOURCE_DIR "${dksdk-cmake_SOURCE_DIR}"
        GIT_REPOSITORY "${_dksdk_cmake_GITREPOSITORY}"
        GIT_TAG "${_dksdk_cmake_GITREF}")
elseif(NOT DKSDK_DISABLE_AUTOACCESS_DKSDK_CMAKE AND NOT CMAKE_SCRIPT_MODE_FILE)
    include(FetchContent)

    # If we have FETCHCONTENT_SOURCE_DIR_DKSDK-CMAKE set, we are configured.
    if(FETCHCONTENT_SOURCE_DIR_DKSDK-CMAKE)
        set(_dksdk-cmake_USER_CONFIGURED_SOMETHING ON)
    endif()

    # --- Warning
    if(NOT _dksdk-cmake_USER_CONFIGURED_SOMETHING)
        message(WARNING "Right now is a good time to configure DkSDK CMake! Follow the DkSDK CMake Subscriber Access Guide at https://diskuv.com/cmake/help/latest/guide/subscriber-access/")
    endif()

    # --- Fetch dksdk-cmake
    FetchContent_Declare(dksdk-cmake # override with cmake -D FETCHCONTENT_SOURCE_DIR_DKSDK-CMAKE=...
        GIT_REPOSITORY "${_dksdk_cmake_GITREPOSITORY}"
        GIT_TAG "${_dksdk_cmake_GITREF}")
    FetchContent_MakeAvailable(dksdk-cmake)
endif()

# Stop final leaky variables
unset(_dksdk_cmake_GITREPOSITORY)
unset(_dksdk_cmake_GITREF)
unset(_dksdk-cmake_USER_CONFIGURED_SOMETHING)
