include_guard()

include("${CMAKE_CURRENT_LIST_DIR}/DkSDKDetectSourceChange.cmake")

#   list command no longer ignores empty elements.
cmake_policy(SET CMP0007 NEW)
#   Included scripts do automatic cmake_policy PUSH and POP.
cmake_policy(SET CMP0011 NEW)
cmake_policy(SET CMP0057 NEW) # Support new IN_LIST if() operator.

macro(_DkSDKCloneSource_GetDirsToCopy_entry)
    string(REGEX REPLACE "/$" "" excludeFileOrDir "${excludeEntry}")
    #   [excludeFileOrDirParentDir] may be the empty string, so CMP0007 is needed
    cmake_path(GET excludeFileOrDir PARENT_PATH excludeFileOrDirParentDir)
endmacro()

# Example EXCLUDES:
#    .ci/cmake/.gitignore
#    .ci/cmake/bin/
#    .ci/cmake/share/
#    .ci/ninja/
#    .ci/ninja/.gitignore
#    .ci/ninja/bin/
#    5.15.2/
#    CMakeUserPresets.json
#    _build/
#    build_dev/
#    fetch/dksdk-access/
#    fetch/opam-repository/
function(DkSDKCloneSource_GetDirsToCopy)
    set(noValues)
    set(singleValues SOURCE_DIR DESTINATION_DIR
        SOURCES_VARIABLE DESTINATIONS_VARIABLE EXCLUDE_SUBENTRIES_VARIABLE)
    set(multiValues EXCLUDES)
    cmake_parse_arguments(PARSE_ARGV 0 ARG "${noValues}" "${singleValues}" "${multiValues}")

    set(sourceDirs)
    set(excludeSubFilesOrDirs)
    set(destinationDirs)
    if(ARG_EXCLUDES)
        # Sort EXCLUDES
        set(excludeEntries ${ARG_EXCLUDES})
        list(SORT excludeEntries)

        # Group by parent directory
        set(excludeEntryParentDirs)
        foreach(excludeEntry IN LISTS excludeEntries)
            _DkSDKCloneSource_GetDirsToCopy_entry()
            #   /PIPE_PREPAD/
            #   Really annoying CMake 3.25.3 bug where list(LENGTH) does not count blank entries.
            #   So prepad with pipes, and remove them later.
            list(APPEND excludeEntryParentDirs "|${excludeFileOrDirParentDir}")
            set("_DkSDKCloneSource_GetDirsToCopy_EXCLUDES_${excludeFileOrDirParentDir}")
        endforeach()
        foreach(excludeEntry IN LISTS excludeEntries)
            _DkSDKCloneSource_GetDirsToCopy_entry()
            cmake_path(GET excludeFileOrDir FILENAME subFileOrDir)
            list(APPEND "_DkSDKCloneSource_GetDirsToCopy_EXCLUDES_${excludeFileOrDirParentDir}" "${subFileOrDir}")
        endforeach()

        # All the ancestors of the parent directories need to be excluded as well. Why? Since we'll
        # be descending down one directory at a time. So iterate until no more changes.
        set(oldParentCount -1)
        set(newParentCount 0)
        while(NOT oldParentCount EQUAL newParentCount)
            list(LENGTH excludeEntryParentDirs oldParentCount)

            # Add parent of parent directory
            foreach(excludeEntryParentDir IN LISTS excludeEntryParentDirs)
                # /PIPE_PREPAD/
                string(REGEX REPLACE "^[|]" "" excludeEntryParentDir "${excludeEntryParentDir}")

                if(excludeEntryParentDir)
                    cmake_path(GET excludeEntryParentDir PARENT_PATH excludeEntryGrandParentDir)
                    cmake_path(GET excludeEntryParentDir FILENAME excludeEntryParentName)
                    #   /PIPE_PREPAD/
                    list(APPEND excludeEntryParentDirs "|${excludeEntryGrandParentDir}")
                    list(APPEND "_DkSDKCloneSource_GetDirsToCopy_EXCLUDES_${excludeEntryGrandParentDir}" "${excludeEntryParentName}")
                    # Cleanups
                    list(REMOVE_DUPLICATES "_DkSDKCloneSource_GetDirsToCopy_EXCLUDES_${excludeEntryGrandParentDir}")
                endif()
            endforeach()

            # Cleanups
            list(REMOVE_DUPLICATES excludeEntryParentDirs)
            list(SORT excludeEntryParentDirs)

            list(LENGTH excludeEntryParentDirs newParentCount)
        endwhile()

        # Iterate over groups
        foreach(excludeEntryParentDir IN LISTS excludeEntryParentDirs)
            # /PIPE_PREPAD/
            string(REGEX REPLACE "^[|]" "" excludeEntryParentDir "${excludeEntryParentDir}")

            if(excludeEntryParentDir)
                cmake_path(APPEND ARG_SOURCE_DIR "${excludeEntryParentDir}" OUTPUT_VARIABLE sourceParentDir)
                cmake_path(APPEND ARG_DESTINATION_DIR "${excludeEntryParentDir}" OUTPUT_VARIABLE thisDestDir)
            else()
                set(sourceParentDir "${ARG_SOURCE_DIR}")
                set(thisDestDir "${ARG_DESTINATION_DIR}")
            endif()

            # Convert list of exclusions into newline separated string
            set(excludeSubList "${_DkSDKCloneSource_GetDirsToCopy_EXCLUDES_${excludeEntryParentDir}}")
            list(JOIN excludeSubList "\n" thisExcludeSubs)

            list(APPEND sourceDirs "${sourceParentDir}")
            list(APPEND excludeSubFilesOrDirs "${thisExcludeSubs}")
            list(APPEND destinationDirs "${thisDestDir}")
        endforeach()
    else()
        # Edge case: No EXCLUDES
        list(APPEND sourceDirs "${ARG_SOURCE_DIR}")
        list(APPEND excludeSubFilesOrDirs "")
        list(APPEND destinationDirs "${ARG_DESTINATION_DIR}")
    endif()

    set("${ARG_SOURCES_VARIABLE}" "${sourceDirs}" PARENT_SCOPE)
    set("${ARG_EXCLUDE_SUBENTRIES_VARIABLE}" "${excludeSubFilesOrDirs}" PARENT_SCOPE)
    set("${ARG_DESTINATIONS_VARIABLE}" "${destinationDirs}" PARENT_SCOPE)
endfunction()

# Testing.
# DkSDKCloneSource_GetDirsToCopy(
#     SOURCE_DIR Y:/source/dksdk-ffi-java
#     DESTINATION_DIR Y:/temp/abc
#     SOURCES_VARIABLE sources
#     DESTINATIONS_VARIABLE destinations
#     EXCLUDE_SUBENTRIES_VARIABLE exclude_subentries
#     EXCLUDES .git/
# )
# message(STATUS "DkSDKCloneSource_GetDirsToCopy results:

# sources=${sources}
# destinations=${destinations}
# exclude_subentries=${exclude_subentries}
# ")

# Deprecated! Poor naming and the INSTALL_PREFIX/NAME notion is too limiting.
function(DkSDKCloneSource_GetLocalDir)
    set(noValues)
    set(singleValues NAME INSTALL_PREFIX SOURCE_DIR)
    set(multiValues)
    cmake_parse_arguments(PARSE_ARGV 0 ARG "${noValues}" "${singleValues}" "${multiValues}")

    message(AUTHOR_WARNING "DkSDKCloneSource_GetLocalDir() is deprecated. Use DkSDKCloneSource_CloneLocalDir() instead")

    if(NOT ARG_INSTALL_PREFIX OR NOT ARG_NAME)
        message(FATAL_ERROR "DkSDKCloneSource_GetLocalDir() requires both INSTALL_PREFIX and NAME arguments. And this function is deprecated! Use DkSDKCloneSource_CloneLocalDir()")
    endif()

    set(destDir "${ARG_INSTALL_PREFIX}/${ARG_NAME}")
    DkSDKCloneSource_CloneLocalDir(
        SOURCE_DIR "${ARG_SOURCE_DIR}"
        DESTINATION_DIR "${destDir}"
    )
endfunction()

function(DkSDKCloneSource_CloneLocalDir)
    set(noValues INCLUDE_DOTGIT IMMUTABLE)
    set(singleValues SOURCE_DIR DESTINATION_DIR OUTPUT_DESTINATION_VARIABLE)
    set(multiValues)
    cmake_parse_arguments(PARSE_ARGV 0 ARG "${noValues}" "${singleValues}" "${multiValues}")

    cmake_path(NORMAL_PATH ARG_SOURCE_DIR OUTPUT_VARIABLE sourceDir)
    cmake_path(NORMAL_PATH ARG_DESTINATION_DIR OUTPUT_VARIABLE destDir)

    if(NOT EXISTS "${sourceDir}")
        message(FATAL_ERROR "A local directory was to be cloned but it does not exist: ${sourceDir}")
    endif()

    if(EXISTS "${sourceDir}/cmake/run/CloneSource.cmake")
        if(ARG_IMMUTABLE)
            message(FATAL_ERROR "Cannot clone from ${sourceDir} to ${destDir} with IMMUTABLE enabled since there is a ${sourceDir}/cmake/run/CloneSource.cmake that does not support IMMUTABLE clones.")
        endif()
        execute_process(
            WORKING_DIRECTORY "${sourceDir}"
            COMMAND "${CMAKE_COMMAND}" -D "CMAKE_INSTALL_PREFIX=${destDir}" -P "${sourceDir}/cmake/run/CloneSource.cmake"
            COMMAND_ERROR_IS_FATAL ANY)
    elseif(IS_DIRECTORY "${sourceDir}/.git")
        if(ARG_IMMUTABLE)
            message(FATAL_ERROR "Cannot clone from ${sourceDir} to ${destDir} with IMMUTABLE enabled since this feature has not yet been developed.")
            # todo: Accumulate DkSDKDetectSourceChange_Checksum() just like CloneSource.cmake.
            # Do the checksum calculation first, and then the copying.
            # For sanity, peel this `if(IS_DIRECTORY .git)` section into separate function.
        endif()
        # Get .gitignore exclusions. Git places a trailing slash on directories.
        find_package(Git QUIET REQUIRED)
        execute_process(
            WORKING_DIRECTORY "${sourceDir}"
            COMMAND "${GIT_EXECUTABLE}" ls-files --exclude-standard -oi --directory
            OUTPUT_VARIABLE gitExcludes
            OUTPUT_STRIP_TRAILING_WHITESPACE
            COMMAND_ERROR_IS_FATAL ANY)
        #   Delete carriage returns (if any)
        string(REPLACE "\r" "" gitExcludes "${gitExcludes}")
        #   Split by newlines
        string(REPLACE "\n" ";" gitExcludes "${gitExcludes}")

        # DESIGN:
        # An earlier version of this used regular expressions [REGEX xxx EXCLUDE]
        # as file(COPY) arguments. That was buggy in CMake 3.25.3. So we use
        # a more straight-forward mechanism ... no regular expressions at all.

        # [.git] is not usually excluded by a [.gitignore]. This is _good_. That means doing a second
        # cmake/run/get.cmake or more generally DkSDKCloneSource_CloneLocalDir() behaves identically
        # to the current cmake/run/get.cmake invocation. The transitivity property is good. But
        # [.git] can be huge so for performance that behavior is opt-in with INCLUDE_DOTGIT argument.
        if(NOT ARG_INCLUDE_DOTGIT)
            list(APPEND gitExcludes ".git/")
        endif()

        DkSDKCloneSource_GetDirsToCopy(
            SOURCE_DIR "${sourceDir}"
            DESTINATION_DIR "${destDir}"
            SOURCES_VARIABLE sources
            DESTINATIONS_VARIABLE destinations
            EXCLUDE_SUBENTRIES_VARIABLE exclude_subentries
            EXCLUDES ${gitExcludes})
        foreach(src src_excludes dst IN ZIP_LISTS sources exclude_subentries destinations)
            # Split exclusions by newlines
            string(REPLACE "\n" ";" src_excludes "${src_excludes}")

            message(VERBOSE "Copying ${src} to ${dst} excluding ${src_excludes}")

            # Since we need to copy the <contents> of <src> to the destination
            # directory, we have to do a glob first.
            file(GLOB dir_CONTENTS LIST_DIRECTORIES TRUE
                RELATIVE "${src}"
                "${src}/*")
            set(dir_abs_INCLUDED)
            foreach(entry IN LISTS dir_CONTENTS)
                if(NOT entry IN_LIST src_excludes)
                    cmake_path(APPEND src "${entry}" OUTPUT_VARIABLE absEntry)
                    list(APPEND dir_abs_INCLUDED "${absEntry}")
                endif()
            endforeach()
            message(VERBOSE "Copying ${src} to ${dst} including ${dir_abs_INCLUDED}")

            # Do file(COPY)
            if(dir_abs_INCLUDED)
                file(COPY ${dir_abs_INCLUDED}
                    DESTINATION "${dst}"
                    NO_SOURCE_PERMISSIONS
                    FOLLOW_SYMLINK_CHAIN)
            endif()
        endforeach()
    else()
        # Fallback to generic CloneSource.cmake
        set(cloneSource_DEFINES)
        set(cloneSource_ID_FILE "${destDir}.id")
        if(ARG_IMMUTABLE)
            list(APPEND cloneSource_DEFINES -D IMMUTABLE=1 -D "IMMUTABLE_ID_OUT_FILE=${cloneSource_ID_FILE}")
        endif()
        execute_process(
            WORKING_DIRECTORY "${sourceDir}"
            COMMAND "${CMAKE_COMMAND}"
                -D "CMAKE_INSTALL_PREFIX=${destDir}"
                -D "CMAKE_MESSAGE_INDENT=${CMAKE_MESSAGE_INDENT} + "
                ${cloneSource_DEFINES}
                -P "${CMAKE_CURRENT_FUNCTION_LIST_DIR}/run/CloneSource.cmake"
            COMMAND_ERROR_IS_FATAL ANY)
        if(ARG_IMMUTABLE)
            file(READ "${cloneSource_ID_FILE}" cloneSource_OUT_ID)
            file(REMOVE "${cloneSource_ID_FILE}")
            set(destDir "${destDir}-${cloneSource_OUT_ID}")
        endif()
    endif()

    # return variables
    if(ARG_OUTPUT_DESTINATION_VARIABLE)
        set(${ARG_OUTPUT_DESTINATION_VARIABLE} "${destDir}" PARENT_SCOPE)
    endif()
endfunction()
