##########################################################################
# File: dksdk-access/cmake/run/CloneSource.cmake                     #
#                                                                        #
# Copyright 2024 Diskuv, Inc.                                            #
#                                                                        #
# Licensed under the DkSDK SOFTWARE DEVELOPMENT KIT LICENSE AGREEMENT    #
# (the "License"); you may not use this file except in compliance        #
# with the License. You may obtain a copy of the License at              #
#                                                                        #
#     https://diskuv.com/legal/                                          #
#                                                                        #
##########################################################################

# TLDR: Consider this to be similar to .gitignore for the entire project.
#
# Summary: Clone the source code of this project.
#
# What: This is both a default template and the actual clone source logic
# for the dksdk-access project.
#
# When: Used when another project expresses a dependency on this project
# inside the dkproject.jsonc configuration file.
#
# When: Used when another project uses this project with
# DkSDKFetchContent_DeclareSecondParty(). For example, another project may
# use this project (pretend it is called "this-project") with the following:
#
#     DkSDKFetchContent_DeclareSecondParty(
#         NAME this-project
#         GIT_REPOSITORY "https://github.com/you/this-project.git"
#         GIT_TAG origin/main)
#     DkSDKFetchContent_MakeAvailableNoInstall(this-project)
#
# How: This script will be run as a
# `cmake -D CMAKE_INSTALL_PREFIX=... -P cmake/run/CloneSource.cmake` command from
# the base of this project (the directory that has ./dk, ./dk.cmd and dune-project).
#
# Consequences: Without this script present ALL of _this_ project can be copied
# into the build folder of _that_ project. Which can be INCREDIBLY
# TIME-CONSUMING when you have huge build folders and so on in this project.
# There is a fallback when `git` can be used to find which files should be
# ignored, but even that fallback can have a multi-second overhead.
#
# Suggestions?
#   Ignore test folders. Why does another project need to run your tests?
#
#   See https://cmake.org/cmake/help/latest/command/file.html#copy-file for
#   all the options you can use.
#
#   For debugging, you should use file(INSTALL ...) rather than file(COPY ...)
#   so you can see what is being copied.

if(NOT CMAKE_INSTALL_PREFIX)
    message(FATAL_ERROR "You cannot run this script without setting CMAKE_INSTALL_PREFIX")
endif()

# Constants
set(EXCLUDED_DIRS .cxx .git .gradle _build)

# Arguments for file(COPY)
set(copy_EXCLUDES)
foreach(exclude IN LISTS EXCLUDED_DIRS)
     list(APPEND copy_EXCLUDES PATTERN "${exclude}" EXCLUDE)
endforeach()

include(${CMAKE_CURRENT_LIST_DIR}/../DkSDKDetectSourceChange.cmake)

# Find files and directories attached to the project root directory
file(GLOB projectRootEntries LIST_DIRECTORIES true
     # Directories
     cmake data dependencies packaging src
     #    Opam
     opam
     #    Gradle
     buildSrc gradle
     # Documentation sometimes needed by packaging/
     *.md
     # dk files
     dk dk.cmd dkproject.jsonc
     # Licenses, if any
     COPYRIGHT LICENSE*
     # CMake files, if any. Never CMakeUserPresets.json since nondeterministic
     CMakeLists.txt CMakePresets.json *.cmake
     # Gradle files, if any
     build.gradle gradle.properties gradlew gradlew.bat settings.gradle
     # Dune files, if any
     *.dune dune dune-project)
#    Accumulate checksum
set(acc_cksum)
if(IMMUTABLE)
     foreach(entry IN LISTS projectRootEntries)
          DkSDKDetectSourceChange_Checksum("${entry}" cksum)
          list(APPEND acc_cksum "${cksum}")
     endforeach()
endif()

# Packaging can use tests/export if it exists
if(IS_DIRECTORY tests/export)
     #    Accumulate checksum
     if(IMMUTABLE)
          DkSDKDetectSourceChange_Checksum(tests/export cksum)
          list(APPEND acc_cksum "${cksum}")
     endif()
endif()

# Add suffix to destination if immutable
if(IMMUTABLE)
     string(CONCAT final_cksum ${acc_cksum})
     string(MD5 final_cksum "${final_cksum}")
     string(SUBSTRING "${final_cksum}" 0 6 id)
     set(destDir "${CMAKE_INSTALL_PREFIX}-${id}")
     message(STATUS "Cloning with immutable id ${id}")
     if(IMMUTABLE_ID_OUT_FILE)
          file(WRITE "${IMMUTABLE_ID_OUT_FILE}" "${id}")
     endif()
else()
     set(destDir "${CMAKE_INSTALL_PREFIX}")
endif()

# Do the actual copying
file(COPY
     ${projectRootEntries}
     DESTINATION "${destDir}"
     NO_SOURCE_PERMISSIONS
     FOLLOW_SYMLINK_CHAIN
     ${copy_EXCLUDES})
if(IS_DIRECTORY tests/export)
     file(COPY
          tests/export
          DESTINATION "${destDir}/tests"
          NO_SOURCE_PERMISSIONS
          FOLLOW_SYMLINK_CHAIN
          ${copy_EXCLUDES})
endif()
