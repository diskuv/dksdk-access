include("${CMAKE_CURRENT_LIST_DIR}/../DkSDKCloneSource.cmake")

function(usage)
    message(FATAL_ERROR [[usage: cmake <options> -P get-one-local.cmake

Options:
    -D NAME=<name>
    -D INSTALL_PREFIX=<installprefix>
    -D SOURCE_DIR=<sourcedir>
]])
endfunction()
if(NOT NAME OR NOT INSTALL_PREFIX OR NOT SOURCE_DIR)
    usage()
endif()

DkSDKCloneSource_GetLocalDir(
    NAME "${NAME}" INSTALL_PREFIX "${INSTALL_PREFIX}" SOURCE_DIR "${SOURCE_DIR}"
)
