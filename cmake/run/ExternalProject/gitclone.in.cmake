# Distributed under the OSI-approved BSD 3-Clause License.  See accompanying
# file Copyright.txt or https://cmake.org/licensing for details.

# DkSDK: Copied this rather than use the Modules files because don't want to be broken based on CMake version changes ...
# this is an internal file
# (https://gitlab.kitware.com/cmake/cmake/-/blob/e1a3752c5e799028d488c51ba2ac54f6f0ddd7da/Modules/ExternalProject/gitclone.cmake.in)
# where the CMake variables can easily change version to version.
#
# DkSDK: Added IS_DIRECTORY test for @source_dir@/.git so source_dir can be deleted and retrigger gitclone.

cmake_minimum_required(VERSION 3.5)

if(IS_DIRECTORY "@source_dir@/.git" AND
  EXISTS "@gitclone_stampfile@" AND EXISTS "@gitclone_infofile@" AND
  "@gitclone_stampfile@" IS_NEWER_THAN "@gitclone_infofile@")
  message(STATUS
    "Avoiding repeated git clone, stamp file is up to date: "
    "'@gitclone_stampfile@'"
  )
  return()
endif()

execute_process(
  COMMAND ${CMAKE_COMMAND} -E rm -rf "@source_dir@"
  RESULT_VARIABLE error_code
)
if(error_code)
  message(FATAL_ERROR "Failed to remove directory: '@source_dir@'")
endif()

# try the clone 3 times in case there is an odd git clone issue
set(error_code 1)
set(number_of_tries 0)
while(error_code AND number_of_tries LESS 3)
  execute_process(
    COMMAND "@git_EXECUTABLE@"
            clone @git_clone_options@ "@git_repository@" "@src_name@"
    WORKING_DIRECTORY "@work_dir@"
    RESULT_VARIABLE error_code
  )
  math(EXPR number_of_tries "${number_of_tries} + 1")
endwhile()
if(number_of_tries GREATER 1)
  message(STATUS "Had to git clone more than once: ${number_of_tries} times.")
endif()
if(error_code)
  message(FATAL_ERROR "Failed to clone repository: '@git_repository@'")
endif()

execute_process(
  COMMAND "@git_EXECUTABLE@"
          checkout "@git_tag@" @git_checkout_explicit--@
  WORKING_DIRECTORY "@work_dir@/@src_name@"
  RESULT_VARIABLE error_code
)
if(error_code)
  message(FATAL_ERROR "Failed to checkout tag: '@git_tag@'")
endif()

set(init_submodules @init_submodules@)
if(init_submodules)
  execute_process(
    COMMAND "@git_EXECUTABLE@" @git_submodules_config_options@
            submodule update @git_submodules_recurse@ --init @git_submodules@
    WORKING_DIRECTORY "@work_dir@/@src_name@"
    RESULT_VARIABLE error_code
  )
endif()
if(error_code)
  message(FATAL_ERROR "Failed to update submodules in: '@work_dir@/@src_name@'")
endif()

# Complete success, update the script-last-run stamp file:
#
execute_process(
  COMMAND ${CMAKE_COMMAND} -E copy "@gitclone_infofile@" "@gitclone_stampfile@"
  RESULT_VARIABLE error_code
)
if(error_code)
  message(FATAL_ERROR "Failed to copy script-last-run stamp file: '@gitclone_stampfile@'")
endif()
