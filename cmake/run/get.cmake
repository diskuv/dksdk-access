cmake_policy(SET CMP0057 NEW) # Support new IN_LIST if() operator.

include("${CMAKE_CURRENT_LIST_DIR}/../DkSDKQuerySource.cmake")

function(usage)
    message(NOTICE [[
usage: cmake <command> [options] -D CONFIG_FILE=dkproject.jsonc -P get.cmake

Dump Variables Command: -D COMMAND_DUMP_VARIABLES=1
    Display variables available to dkproject.jsonc.

    The DKSDK_VERSION_MAJMIN and DKSDK_VERSION variables are based on the name of the
    dependency; because of that the values shown for those two variables
    does not reflect what will be available to a dependency.

    -D SANDBOX=1: Deprecated. Skip the ${sourceParentDir} and
        ${projectParentDir} variables.

Dump Dependencies Command: -D COMMAND_DUMP_DEPENDENCIES=1
    Display dependencies defined in dkproject.jsonc.
    The URLs, including credentials, are not obfuscated so do not capture the
    output in CI logs without obfuscation.

Dump Source Command: -D COMMAND_DUMP_SOURCE=<source>
    Displays the source named <source> defined in dkproject.jsonc.
    Mirroring is performed if the source has a mirror.

Get Command: -D COMMAND_GET=<getdir> -D CACHE_DIR=<cachedir>
    Get the project dependencies and place them in <getdir>, using
    the <cachedir> as an intermediate directory.

    The project dependencies are checked for updates each COMMAND_GET.
    There are some edge cases where changes are not detected (ex. you use a
    branch rather than a commit id in a Git-based dependency).
    Deleting the <cachedir> will force the corresponding <getdir>-s
    to be updated the next COMMAND_GET.

    Options:
    -D INTERACTIVE=1: Allow terminal prompts for git username and password.
        The Ninja program, which can place commands in a "console" job
        pool, is required for correct functioning.

    -D SANDBOX=1: Deprecated. Skip any URLs that use ${sourceParentDir} or
        ${projectParentDir}.

    -D OUT_DKSDK_VERSION=<file>: Use the dksdk-cmake dependency to place the
       Major.Minor.Patch version that would be reported by
       DkSDKVersion_GetVersion() into the specified file.
       For example, when the dksdk-cmake dependency is the "next" branch of
       dksdk-cmake, the version would be the _unreleased_ version of DkSDK.
       The typical scenario is performing CI jobs for any of the DkSDK packages
       (ex. dksdk-ffi-java, dksdk-ffi-ocaml); these CI jobs need to know the
       version of DkSDK they are building so that their builds can be configured.
       For example:
       - dksdk-ffi-java needs to configure Gradle settings since the DkSDK
         version is a fundamental part of the JAR naming
       - dksdk-ffi-ocaml, which could invoke DkSDKVersion_GetVersion() itself,
         has publishing CI jobs that shouldn't need a lengthy `cmake -G` of
         dksdk-ffi-ocaml just to calculate the the DkSDK versioned REST URLs
         to POST artifacts

    -D OUT_DKSDK_VERSION_MAJMIN=<file>: Same as OUT_DKSDK_VERSION except
        Major.Minor format.
    
    -D ONLY_DKSDK_VERSION=1: Only fetch enough dependencies to calculate
       OUT_DKSDK_VERSION and OUT_DKSDK_VERSION_MAJMIN accurately. Currently
       the dksdk-cmake dependency is the only dependency needed.

Common Options
    -D CONFIG_FILE=dkproject.jsonc: Required. The file .z-dk/project-variant,
        if present in the same directory as the CONFIG_FILE, will override
        the dkproject.jsonc selection. For example, if
        `-D CONFIG_FILE=a/b/dkproject.jsonc` then `a/b/.z-dk/project-variant`
        is checked. The contents of .z-dk/project-variant will be the
        location of configuration file; relative paths are interpreted relative
        to the directory containing the CONFIG_FILE.

    -D SOURCE_DIR=<dir>: Assign the directory <dir> as ${sourceDir}, and
        the parent directory of <dir> as ${sourceParentDir}.
        The SOURCE_DIR may start with a tilde (~) which will be treated
        as the home directory on Unix or the USERPROFILE directory on
        Windows.
        If SOURCE_DIR is a relative path it will be relative to the
        directory containing `dkproject.jsonc`.
        Defaults to the directory containing `dkproject.jsonc`.

    -D PROJECT_DIR=<dir>: Assign the directory <dir> as ${projectDir}, and
        the parent directory of <dir> as ${projectParentDir}.
        The PROJECT_DIR may start with a tilde (~) which will be treated
        as the home directory on Unix or the USERPROFILE directory on
        Windows.
        If PROJECT_DIR is a relative path it will be relative to the
        directory containing `dkproject.jsonc`.
        Defaults to the directory containing `dkproject.jsonc`.

    -D OVERRIDE_DKSDK_VERSION=<version>: Override the DKSDK_VERSION and
        DKSDK_VERSION_MAJMIN variables. The <version> must be in the Major.Minor.Patch
        format.
    
    -D DKSDK_CMAKE_GITREF=<ref>: Set the dksdk-cmake_GITREF variable. The
        default is `main`.

Standard Variables available to dkproject.jsonc:

    ${sourceDir}: The source directory. By default it is the directory
    containing `dkproject.jsonc`.

    ${sourceParentDir}: The parent directory of ${sourceDir}.

    ${dksdk-cmake_GITREF}: A Git branch, tag or commit that can be used like:
    
      "dksdk-cmake": { "urls": [ "${dksdk-cmake_REPOSITORY}#${dksdk-cmake_GITREF}" ] }
    
    The default is `main`.

    ${DKSDK_VERSION}: The Major.Minor.Patch format of the DkSDK version.
    The version is the value of the -D DKSDK_VERSION, if specified.
    Failing that, if the dependency being evaluated is not `dksdk-cmake`
    and there is a `dksdk-cmake` dependency in the project configuration,
    the version is calculated so it is the equivalent of
    DkSDKVersion_GetVersion() from the dksdk-cmake dependency.
    Failing that, the value is 0.0.0.

    ${DKSDK_VERSION_MAJMIN}: The Major.Minor format of the DkSDK version.
    The version is the first two Major.Minor parts of the -D DKSDK_VERSION,
    if specified.
    Failing that, if the dependency being evaluated is not `dksdk-cmake`
    and there is a `dksdk-cmake` dependency in the project configuration,
    the version is calculated so it is the equivalent of
    DkSDKVersion_GetVersion() from the dksdk-cmake dependency.
    Failing that, the value is 0.0.
]])
endfunction()

if(NOT CONFIG_FILE)
    usage()
    return()
endif()

# Determine directory containing the CONFIG_FILE (dkproject.jsonc)
cmake_path(ABSOLUTE_PATH CONFIG_FILE NORMALIZE OUTPUT_VARIABLE _configFileAbs)
cmake_path(GET _configFileAbs PARENT_PATH _configFileAbsDir)

# Project variant
set(_projectVariant)
set(_variantConfigFile "${_configFileAbsDir}/.z-dk/project-variant")
if(EXISTS "${_variantConfigFile}")
    # First line
    file(STRINGS "${_variantConfigFile}" _projectVariant LIMIT_COUNT 1)
    # Make _projectVariant an absolute path if it isn't already
    cmake_path(IS_RELATIVE _projectVariant _projectVariantIsRelative)
    if(_projectVariantIsRelative)
        cmake_path(APPEND _configFileAbsDir "${_projectVariant}" OUTPUT_VARIABLE _projectVariant)
    endif()
    message(STATUS "Using project configuration `${_projectVariant}` set in `${_variantConfigFile}`. Sandboxing is off.")
endif()
if(_projectVariant)
    set(config_ACTIVEFILE "${_projectVariant}")
    set(config_REQUIRE_SANDBOX OFF)
else()
    # No project variant means that sandboxing must be on.
    set(config_ACTIVEFILE "${CONFIG_FILE}")
    set(config_REQUIRE_SANDBOX ON)
endif()
unset(_projectVariant)
unset(_variantConfigFile)
unset(_projectVariantIsRelative)

# DKSDK_CMAKE_GITREF
set(dksdk-cmake_GITREF main)
if(DKSDK_CMAKE_GITREF)
    set(dksdk-cmake_GITREF "${DKSDK_CMAKE_GITREF}")
endif()

# Compute: source dir and project dir
if(SOURCE_DIR)
    file(REAL_PATH "${SOURCE_DIR}" sourceDir BASE_DIRECTORY "${_configFileAbsDir}" EXPAND_TILDE) # Compared to cmake_path(ABSOLUTE_PATH) any trailing slash is removed and file(GET PARENT_PATH) works correctly.
else()
    # default is the directory containing the CONFIG_FILE (dkproject.jsonc)
    set(sourceDir "${_configFileAbsDir}")
endif()
if(PROJECT_DIR)
    file(REAL_PATH "${PROJECT_DIR}" projectDir BASE_DIRECTORY "${_configFileAbsDir}" EXPAND_TILDE) # Compared to cmake_path(ABSOLUTE_PATH) any trailing slash is removed and file(GET PARENT_PATH) works correctly.
else()
    # default is the directory containing the CONFIG_FILE (dkproject.jsonc)
    set(projectDir "${_configFileAbsDir}")
endif()

# Sanitize: git_auth_RETRIES := DKSDK_ACCESS_GIT_AUTH_RETRIES between 1 and 5
set(_git_auth_RETRIES 3)
if("$ENV{DKSDK_ACCESS_GIT_AUTH_RETRIES}" GREATER_EQUAL 1 AND "$ENV{DKSDK_ACCESS_GIT_AUTH_RETRIES}" LESS_EQUAL 5)
    set(_git_auth_RETRIES $ENV{DKSDK_ACCESS_GIT_AUTH_RETRIES})
endif()

# Interactive
set(_interactivity_PRELUDE)
set(_interactivity_GITOPTIONS)
set(_interactivity_EXTERNALPROJECTADD_ARGS
    USES_TERMINAL_CONFIGURE TRUE)
if(NOT INTERACTIVE)
    set(_git_auth_RETRIES 1) # No point retrying if not interactive!
    # Set "GIT_ASKPASS" to some program which prints nothing on stdout, has exit code 0, and is quick.
    #
    # Sigh ... no good program like `true` for Windows ...
    #
    # if(CMAKE_HOST_WIN32)
    #     # C:\Windows\System32\where.exe
    #     find_program(askpass_NOOP NAMES where PATHS "$ENV{SystemRoot}/Sysnative" "$ENV{SystemRoot}/System32" NO_DEFAULT_PATH REQUEST_DKSDK_VERSION)
    #     # C:\Windows\System32
    #     cmake_path(GET askpass_NOOP PARENT_PATH askpass_NOOP_DIR)
    #     # C:\Windows\System32\where.exe /Q C:/Windows/System32:where.exe
    #     set(askpass_NOOP "\"${askpass_NOOP}\" /Q ${askpass_NOOP_DIR}:where.exe")
    #     message(NOTICE "askpass_NOOP = ${askpass_NOOP}")
    # else()
    #     # /usr/bin/true
    #     find_program(askpass_NOOP NAMES true PATHS /usr/bin /bin NO_DEFAULT_PATH REQUIRED)
    # endif()
    set(_interactivity_PRELUDE ${CMAKE_COMMAND} -E env GIT_TERMINAL_PROMPT=false --)
    set(_interactivity_GITOPTIONS -c credential.helper=)
    set(_interactivity_EXTERNALPROJECTADD_ARGS)
endif()

# Use DkSDKQuerySource_Init to define expansion variables and REQUEST_DKSDK_VERSION
if(SANDBOX)
    message(WARNING "The SANDBOX option is deprecated. Use .z-dk/project.variant to control which variant of the dkproject.jsonc you want active. Whatever is calling SANDBOX mode should instead remove the .z-dk/ folder so no variants are possible.")
endif()
if(SANDBOX OR config_REQUIRE_SANDBOX)
    set(_querySource_ARGS SANDBOX)
else()
    set(_querySource_ARGS)
endif()
DkSDKQuerySource_Init(
    CONFIG_FILE "${config_ACTIVEFILE}"
    CONTENT_VARIABLE config_CONTENT
    SOURCE_DIR "${sourceDir}"
    PROJECT_DIR "${projectDir}"
    ${_querySource_ARGS})

# Use DkSDKAccess to define access variables (uses REQUEST_DKSDK_VERSION)
include(${CMAKE_CURRENT_LIST_DIR}/../DkSDKAccess.cmake)

macro(print_all_variables WHERE)
    message(NOTICE "BEGIN ${WHERE}-----------------------")
    get_cmake_property(_variableNames VARIABLES)
    foreach (_variableName ${_variableNames})
        if(NOT (_variableName IN_LIST EXCLUDE_VARIABLES)
            AND NOT (_variableName MATCHES "^CMAKE_.*")
            AND NOT (_variableName MATCHES "^_.*"))
            set(_value ${${_variableName}})

            # More exclusions
            if(DkSDKQuerySource_NOT_IN_SANDBOX AND _value MATCHES "${DkSDKQuerySource_NOT_IN_SANDBOX}")
                continue()
            endif()

            # Padding
            string(LENGTH "${_variableName}" _nameLen)
            math(EXPR _padRight "40 - ${_nameLen}")
            set(_pad "")
            if(_padRight GREATER 0)
                string(REPEAT " " ${_padRight} _pad)
            endif()

            # Secrets cloaking
            if(_variableName MATCHES "_REPOSITORY")
                # https://dksdk-access:sometoken@gitlab.com/diskuv/distributions/1.0/dksdk-cmake.git
                # to https://redacted:redacted@gitlab.com/diskuv/distributions/1.0/dksdk-cmake.git
                string(REGEX REPLACE "://[^/]*@" "://redacted:redacted@" _value "${_value}")
            endif()

            message(NOTICE "${_variableName}${_pad} = ${_value}")
        endif()
    endforeach()
    message(NOTICE "END   ${WHERE}-----------------------")
endmacro()

function(do_get_git)
    set(noValues)
    set(singleValues NAME INSTALL_PREFIX CACHE_DIR REPOSITORY REF)
    set(multiValues)
    cmake_parse_arguments(PARSE_ARGV 0 ARG "${noValues}" "${singleValues}" "${multiValues}")

    set(destDir "${ARG_INSTALL_PREFIX}/${ARG_NAME}")
    cmake_path(NORMAL_PATH destDir)

    # Very weird that the following "works" but has broken Git authentication in interactive
    # mode. Must be a side-effect of using MSBuild or Ninja (both tested on Windows) as an
    # intermediate process which is launching the git command (yes, using "console" in Ninja).
    #
    #     set(fetchcontent_EXTRA_ARGS)
    #     if(CMAKE_MAKE_PROGRAM)
    #         cmake_path(NORMAL_PATH CMAKE_MAKE_PROGRAM OUTPUT_VARIABLE CMAKE_MAKE_PROGRAM_NORMAL)
    #         set(CMAKE_GENERATOR Ninja)
    #     endif()
    #
    #     include(FetchContent)
    #     FetchContent_Populate("${ARG_NAME}"
    #         QUIET
    #         GIT_REPOSITORY "${ARG_REPOSITORY}"
    #         GIT_TAG "${ARG_REF}"
    #         # Disabled because _probably_ must be _must_ or get failures ...
    #         #
    #         # > As of 3.25.3 the bug https://gitlab.kitware.com/cmake/cmake/-/issues/24578
    #         # > has still not been fixed. That means empty strings get removed.
    #         # > ExternalProject_Add(GIT_SUBMODULES) in dktool-subbuild/CMakeLists.txt
    #         # > means fetch all submodules.
    #         # > https://gitlab.kitware.com/cmake/cmake/-/issues/20579#note_734045
    #         # > has a workaround.
    #         # GIT_SUBMODULES src # Non-git-submodule dir that _probably_ already exists
    #         # GIT_SUBMODULES_RECURSE OFF
    #         SUBBUILD_DIR    "${ARG_CACHE_DIR}/${ARG_NAME}-subbuild"
    #         SOURCE_DIR      "${destDir}"
    #         BINARY_DIR      "${ARG_CACHE_DIR}/${ARG_NAME}-build"
    #
    #         # USES_TERMINAL_CONFIGURE TRUE in interactive mode
    #         ${_interactivity_EXTERNALPROJECTADD_ARGS})
    #
    # So ... let's short-circuit all of that and just launch the git populate command directly!

    #   Directory to hold our content
    string(SHA256 populateId "${ARG_NAME}:${ARG_INSTALL_PREFIX}")
    string(SUBSTRING "${populateId}" 1 8 populateId)

    #   Template variables
    find_package(Git QUIET REQUIRED)
    set(git_EXECUTABLE "${GIT_EXECUTABLE}") # yes, mixed case naming is needed
    set(git_remote_name origin)
    if(ARG_REF)
        set(git_tag ${ARG_REF})
    else()
        set(git_tag main) # The FetchContent/ExternalProject_Add default is "master" but that is rapidly losing favor
    endif()
    set(git_update_strategy REBASE_CHECKOUT)
    set(git_stash_save_options --quiet --include-untracked)
    set(init_submodules FALSE)
    set(git_submodules_config_options)
    set(git_submodules_recurse --recursive)
    set(git_submodules)
    #       using NAME to identify log message
    set(gitclone_stampfile "${ARG_CACHE_DIR}/${populateId}/${ARG_NAME}-gitclone-lastrun.txt")
    set(gitclone_infofile "${ARG_CACHE_DIR}/${populateId}/gitinfo.txt")
    set(source_dir "${destDir}")
    set(git_clone_options "--no-checkout --config \"advice.detachedHead=false\"")
    set(git_repository "${ARG_REPOSITORY}")
    set(src_name "${ARG_NAME}")
    set(git_checkout_explicit-- --)
    set(method git)
    set(extra_repo_info "
repository=${git_repository}
remote=${git_remote_name}
init_submodules=${init_submodules}
recurse_submodules=${git_submodules_recurse}
submodules=${git_submodules}")

    #   [RepositoryInfo.txt] and [gitclone]
    set(cmd "${CMAKE_COMMAND}" -P "${ARG_CACHE_DIR}/${populateId}/gitclone.cmake")
    set(work_dir "${ARG_INSTALL_PREFIX}")
    file(MAKE_DIRECTORY "${ARG_INSTALL_PREFIX}")
    configure_file("${CMAKE_CURRENT_FUNCTION_LIST_DIR}/ExternalProject/RepositoryInfo.in.txt" "${gitclone_infofile}" @ONLY)
    configure_file("${CMAKE_CURRENT_FUNCTION_LIST_DIR}/ExternalProject/gitclone.in.cmake" "${ARG_CACHE_DIR}/${populateId}/gitclone.cmake" @ONLY)
    #   This nested execute_process() used by ExternalProject_Add() is a waste of time!
    #     execute_process(
    #         COMMAND ${cmd}
    #         COMMAND_ERROR_IS_FATAL ANY
    #     )
    include("${ARG_CACHE_DIR}/${populateId}/gitclone.cmake")

    #   [gitupdate]
    set(work_dir "${destDir}")
    configure_file("${CMAKE_CURRENT_FUNCTION_LIST_DIR}/ExternalProject/gitupdate.in.cmake" "${ARG_CACHE_DIR}/${populateId}/gitupdate.cmake" @ONLY)
    #   This nested execute_process() used by ExternalProject_Add() is a waste of time!
    #     execute_process(
    #         COMMAND
    #         "${CMAKE_COMMAND}" -P "${ARG_CACHE_DIR}/${populateId}/gitupdate.cmake"
    #         COMMAND_ERROR_IS_FATAL ANY
    #     )
    #
    #   UPDATE_DISCONNECTED=1 means never updating from a remote repository.
    #       We want the opposite: to fetch from remote repositories.
    set(can_fetch TRUE)
    include("${ARG_CACHE_DIR}/${populateId}/gitupdate.cmake")
endfunction()

macro(json_text_literal input_variable output_variable)
    set(_jsontext "${${input_variable}}")
    string(REPLACE "\\" "\\\\" _jsontext "${_jsontext}") # escape backslash
    string(REPLACE "\"" "\\\"" _jsontext "${_jsontext}") # escape double-quotes
    set(${output_variable} "\"${_jsontext}\"")
    unset(_jsontext)
endmacro()

function(do_dump_dependency)
    set(noValues)
    set(singleValues NAME OUTPUT_VARIABLE)
    set(multiValues)
    cmake_parse_arguments(PARSE_ARGV 0 ARG "${noValues}" "${singleValues}" "${multiValues}")

    get_property(type GLOBAL PROPERTY dependency_${ARG_NAME})
    get_property(git_repository GLOBAL PROPERTY dependency_${ARG_NAME}_GIT_REPOSITORY)
    get_property(git_ref GLOBAL PROPERTY dependency_${ARG_NAME}_GIT_REF)
    get_property(local_dir GLOBAL PROPERTY dependency_${ARG_NAME}_LOCAL_DIR)
    get_property(local_mirror_from GLOBAL PROPERTY dependency_${ARG_NAME}_LOCAL_MIRROR_FROM)
    get_property(local_mirror_to GLOBAL PROPERTY dependency_${ARG_NAME}_LOCAL_MIRROR_TO)
    get_property(local_mirror_include_dotgit GLOBAL PROPERTY dependency_${ARG_NAME}_LOCAL_MIRROR_INCLUDE_DOTGIT)
    get_property(local_mirror_immutable GLOBAL PROPERTY dependency_${ARG_NAME}_LOCAL_MIRROR_IMMUTABLE)
    get_property(urls GLOBAL PROPERTY dependency_${ARG_NAME}_URLS)
    list(LENGTH urls urls_LEN)

    # JSON field naming? Use camelCase.
    #   https://stackoverflow.com/questions/5543490/json-naming-convention-snake-case-camelcase-or-pascalcase
    #   We give a slight nod to JavaScript as the originator of JSON, so choose camelCase or snake_case.
    json_text_literal(ARG_NAME name_LIT)
    json_text_literal(type type_LIT)
    json_text_literal(git_repository git_repository_LIT)
    json_text_literal(git_ref git_ref_LIT)
    json_text_literal(local_dir local_dir_LIT)
    json_text_literal(local_mirror_from local_mirror_src_LIT)
    json_text_literal(local_mirror_to local_mirror_dst_LIT)
    set(local_mirror_include_dotgit_LIT false)
    set(local_mirror_immutable_LIT false)
    if(local_mirror_include_dotgit)
        set(local_mirror_include_dotgit_LIT true)
    endif()
    if(local_mirror_immutable)
        set(local_mirror_immutable_LIT true)
    endif()
    set(urls_LIT "[]")
    foreach(url IN LISTS urls)
        json_text_literal(url url_LIT)
        string(JSON urls_LIT SET "${urls_LIT}" ${urls_LEN} "${url_LIT}")
    endforeach()

    set(jsonObject "{}")
    string(JSON jsonObject SET "${jsonObject}" name "${name_LIT}")
    string(JSON jsonObject SET "${jsonObject}" type "${type_LIT}")
    string(JSON jsonObject SET "${jsonObject}" candidateUrls "${urls_LIT}")
    if(type STREQUAL GIT_HTTPS)
        string(JSON jsonObject SET "${jsonObject}" gitRepository "${git_repository_LIT}")
        string(JSON jsonObject SET "${jsonObject}" gitRef "${git_ref_LIT}")
    elseif(type STREQUAL GIT_SSH)
        string(JSON jsonObject SET "${jsonObject}" gitRepository "${git_repository_LIT}")
        string(JSON jsonObject SET "${jsonObject}" gitRef "${git_ref_LIT}")
    elseif(type STREQUAL LOCAL_DIR)
        string(JSON jsonObject SET "${jsonObject}" localDir "${local_dir_LIT}")
    elseif(type STREQUAL LOCAL_MIRROR)
        string(JSON jsonObject SET "${jsonObject}" localMirrorFrom "${local_mirror_src_LIT}")
        string(JSON jsonObject SET "${jsonObject}" localMirrorTo "${local_mirror_dst_LIT}")
        string(JSON jsonObject SET "${jsonObject}" localMirrorIncludeDotGit "${local_mirror_include_dotgit_LIT}")
        string(JSON jsonObject SET "${jsonObject}" localMirrorImmutable "${local_mirror_immutable_LIT}")
    elseif(NOT type)
        message(FATAL_ERROR "The type for dependency `${ARG_NAME}` was not set")
    elseif(NOT type)
        message(FATAL_ERROR "The type `${type}` for dependency `${ARG_NAME}` is unsupported")
    endif()
    set(${ARG_OUTPUT_VARIABLE} "${jsonObject}" PARENT_SCOPE)
endfunction()

function(do_get)
    set(noValues)
    set(singleValues NAME INSTALL_PREFIX CACHE_DIR)
    set(multiValues)
    cmake_parse_arguments(PARSE_ARGV 0 ARG "${noValues}" "${singleValues}" "${multiValues}")

    get_property(type GLOBAL PROPERTY dependency_${ARG_NAME})
    get_property(git_repository GLOBAL PROPERTY dependency_${ARG_NAME}_GIT_REPOSITORY)
    get_property(git_ref GLOBAL PROPERTY dependency_${ARG_NAME}_GIT_REF)
    get_property(local_dir GLOBAL PROPERTY dependency_${ARG_NAME}_LOCAL_DIR)
    get_property(local_mirror_from GLOBAL PROPERTY dependency_${ARG_NAME}_LOCAL_MIRROR_FROM)
    get_property(local_mirror_to GLOBAL PROPERTY dependency_${ARG_NAME}_LOCAL_MIRROR_TO)
    get_property(local_mirror_include_dotgit GLOBAL PROPERTY dependency_${ARG_NAME}_LOCAL_MIRROR_INCLUDE_DOTGIT)
    get_property(local_mirror_immutable GLOBAL PROPERTY dependency_${ARG_NAME}_LOCAL_MIRROR_IMMUTABLE)

    include("${CMAKE_CURRENT_LIST_DIR}/../DkSDKCloneSource.cmake")

    set(common_ARGS NAME "${ARG_NAME}" INSTALL_PREFIX "${ARG_INSTALL_PREFIX}")
    if(type STREQUAL GIT_HTTPS)
        message(STATUS "Getting ${ARG_NAME} from git+https")
        do_get_git(${common_ARGS} REPOSITORY "${git_repository}" REF "${git_ref}" CACHE_DIR "${ARG_CACHE_DIR}")
    elseif(type STREQUAL GIT_SSH)
        message(STATUS "Getting ${ARG_NAME} from git+ssh")
        do_get_git(${common_ARGS} REPOSITORY "${git_repository}" REF "${git_ref}" CACHE_DIR "${ARG_CACHE_DIR}")
    elseif(type STREQUAL LOCAL_MIRROR)
        message(STATUS "Cloning ${ARG_NAME} source code from ${local_mirror_from} to ${local_mirror_to}")
        set(clone_ARGS)
        if(local_mirror_include_dotgit)
            list(APPEND clone_ARGS INCLUDE_DOTGIT)
        endif()
        if(local_mirror_immutable)
            list(APPEND clone_ARGS IMMUTABLE)
        endif()
        DkSDKCloneSource_CloneLocalDir(
            SOURCE_DIR "${local_mirror_from}"
            DESTINATION_DIR "${local_mirror_to}"
            ${clone_ARGS})
    elseif(type STREQUAL LOCAL_DIR)
        message(STATUS "Assigning ${ARG_NAME} to ${local_dir}")
    elseif(NOT type)
        message(FATAL_ERROR "The type for dependency `${ARG_NAME}` was not set")
    elseif(NOT type)
        message(FATAL_ERROR "The type `${type}` for dependency `${ARG_NAME}` is unsupported")
    endif()
endfunction()

# Initial version calculation
if(OVERRIDE_DKSDK_VERSION)
    string(REPLACE "." ";" _version_splat "${OVERRIDE_DKSDK_VERSION}")
    list(GET _version_splat 0 _maj)
    list(GET _version_splat 1 _min)
    list(GET _version_splat 2 _pat)
    set(DKSDK_VERSION "${_maj}.${_min}.${_pat}")
    set(DKSDK_VERSION_MAJMIN "${_maj}.${_min}")
else()
    set(DKSDK_VERSION 0.0.0)
    set(DKSDK_VERSION_MAJMIN 0.0)
endif()

# Don't leak variables
foreach(_v IN ITEMS CONFIG_FILE)
    unset(${_v} CACHE)
endforeach()
foreach(_v IN ITEMS REQUEST_DKSDK_VERSION REQUEST_DKSDK_VERSION_MAJOR REQUEST_DKSDK_VERSION_MINOR)
    unset(${_v})
endforeach()

# Perform -D COMMAND_DUMP_VARIABLES
if(COMMAND_DUMP_VARIABLES)
    set(EXCLUDE_VARIABLES
        WIN32 UNIX APPLE LINUX
        DkSDKQuerySource_NOT_IN_SANDBOX EXCLUDE_VARIABLES
        # commands
        COMMAND_DUMP_VARIABLES COMMAND_DUMP_DEPENDENCIES COMMAND_GET
        # command line options
        SANDBOX INTERACTIVE SOURCE_DIR PROJECT_DIR CACHE_DIR
        OVERRIDE_DKSDK_VERSION OUT_DKSDK_VERSION
        # others
        config_CONTENT config_ACTIVEFILE config_REQUIRE_SANDBOX
        DKSDK_DATA_HOME)
    print_all_variables("variables ")
    return()
endif()

# Perform -D COMMAND_DUMP_DEPENDENCIES
if(COMMAND_DUMP_DEPENDENCIES)
    # Parse the project
    DkSDKQuerySource_ParseConfig(
        NETWORK VALIDATE
        CONTENT "${config_CONTENT}"
        DEPENDENCIES_VARIABLE dependencies
        GIT_AUTH_RETRIES "${git_auth_RETRIES}"
    )

    # [dump-dependency] all the explicit dependencies
    set(dependencyArray "[]")
    list(LENGTH dependencies dependenciesLen)
    foreach(dependency IN LISTS dependencies)
        do_dump_dependency(NAME "${dependency}" OUTPUT_VARIABLE dependencyObject)
        string(JSON dependencyArray SET "${dependencyArray}" ${dependenciesLen} "${dependencyObject}") # append to end
    endforeach()
    message(NOTICE "${dependencyArray}")

    return()
endif()

# Perform -D COMMAND_DUMP_SOURCE
if(COMMAND_DUMP_SOURCE)
    DkSDKQuerySource_FindSource(
        CONTENT "${config_CONTENT}"
        NAME "${COMMAND_DUMP_SOURCE}"
        DOWNLOAD_OPTIONS_VARIABLE downloadOptions
        SUCCESS_VARIABLE success)

    if(success)
        list(JOIN downloadOptions " " downloadOptionsText)
        message(NOTICE "FetchContent_Declare(${COMMAND_DUMP_SOURCE} ${downloadOptionsText})")
    else()
        message(FATAL_ERROR "The dependency ${COMMAND_DUMP_SOURCE} was not found")
    endif()

    return()
endif()

# Perform -D COMMAND_GET -D CACHE_DIR
if(COMMAND_GET)
    if(NOT CACHE_DIR)
        message(NOTICE "Missing -D CACHE_DIR=<cachedir> option")
        usage()
        message(NOTICE "Missing -D CACHE_DIR=<cachedir> option")
        return()
    endif()

    # Normalize destination and cache dir
    cmake_path(NORMAL_PATH COMMAND_GET OUTPUT_VARIABLE installPrefix)
    cmake_path(NORMAL_PATH CACHE_DIR OUTPUT_VARIABLE cacheDir)

    # Parse the project. We don't know yet if it includes dksdk-cmake, so if necessary for calculating DkSDK version just check for dksdk-cmake.
    set(_initialParse_ARGS)    
    if(ONLY_DKSDK_VERSION OR NOT OVERRIDE_DKSDK_VERSION)
        list(APPEND _initialParse_ARGS ONLY_DKSDK_CMAKE)
    endif()
    DkSDKQuerySource_ParseConfig(
        NETWORK VALIDATE
        CONTENT "${config_CONTENT}"
        DEPENDENCIES_VARIABLE dependencies
        GIT_AUTH_RETRIES "${git_auth_RETRIES}"
        ${_initialParse_ARGS})

    # Do the dependencies include dksdk-cmake? If not, can't do OUT_DKSDK_VERSION (unless it was overridden)
    if(OUT_DKSDK_VERSION AND NOT "dksdk-cmake" IN_LIST dependencies AND NOT OVERRIDE_DKSDK_VERSION)
        message(FATAL_ERROR "The project configuration ${config_ACTIVEFILE} does not have the `dksdk-cmake` dependency and OVERRIDE_DKSDK_VERSION was not set. That means the DkSDK version cannot be calculated, and the OUT_DKSDK_VERSION option cannot be used.")
    endif()
    if(OUT_DKSDK_VERSION_MAJMIN AND NOT "dksdk-cmake" IN_LIST dependencies AND NOT OVERRIDE_DKSDK_VERSION)
        message(FATAL_ERROR "The project configuration ${config_ACTIVEFILE} does not have the `dksdk-cmake` dependency and OVERRIDE_DKSDK_VERSION was not set. That means the DkSDK version cannot be calculated, and the OUT_DKSDK_VERSION_MAJMIN option cannot be used.")
    endif()

    # Add .gitignore and dune to <installprefix>
    DkSDKQuerySource_PrepareInstallPrefixDirectory(
        INSTALL_PREFIX "${installPrefix}"
    )
    
    # If dksdk-cmake is configured in the project configuration and we don't
    # have a DkSDK version, we need to download dksdk-cmake so we can get the
    # DkSDK version, and then re-initialize all the variables.
    set(_already_got_dksdk_cmake OFF)
    if("dksdk-cmake" IN_LIST dependencies AND NOT OVERRIDE_DKSDK_VERSION)
        # Fetch dksdk-cmake
        do_get(NAME "dksdk-cmake" INSTALL_PREFIX "${installPrefix}" CACHE_DIR "${cacheDir}")
        # Get its version information and place in DKSDK_VERSION_MAJMIN and DKSDK_VERSION
        include("${installPrefix}/dksdk-cmake/cmake/DkSDKVersion.cmake")
        DkSDKVersion_GetVersion(OUTPUT_VERSION DKSDK_VERSION
            OUTPUT_VERSION_MAJOR _maj
            OUTPUT_VERSION_MINOR _min
        )
        set(DKSDK_VERSION_MAJMIN "${_maj}.${_min}")
        # Re-initialize
        DkSDKQuerySource_Init(
            CONFIG_FILE "${config_ACTIVEFILE}"
            CONTENT_VARIABLE config_CONTENT
            SOURCE_DIR "${sourceDir}"
            PROJECT_DIR "${projectDir}"
            ${_querySource_ARGS})
        # Re-parse the URLs for the project, not just dksdk-cmake
        DkSDKQuerySource_ParseConfig(
            NETWORK VALIDATE
            CONTENT "${config_CONTENT}"
            DEPENDENCIES_VARIABLE dependencies
            GIT_AUTH_RETRIES "${git_auth_RETRIES}")
        # Don't do dksdk-cmake again
        set(_already_got_dksdk_cmake ON)
    endif()

    # At this point we have the final DkSDK version number.
    # We can output the dksdk version file, if any.
    if(OUT_DKSDK_VERSION)
        cmake_path(GET OUT_DKSDK_VERSION PARENT_PATH _versionDir)
        file(MAKE_DIRECTORY "${_versionDir}")
        file(WRITE "${OUT_DKSDK_VERSION}" "${DKSDK_VERSION}")
    endif()
    if(OUT_DKSDK_VERSION_MAJMIN)
        cmake_path(GET OUT_DKSDK_VERSION_MAJMIN PARENT_PATH _versionDir)
        file(MAKE_DIRECTORY "${_versionDir}")
        file(WRITE "${OUT_DKSDK_VERSION_MAJMIN}" "${DKSDK_VERSION_MAJMIN}")
    endif()

    # If we need more than the DkSDK version number, get the other dependencies
    if(NOT ONLY_DKSDK_VERSION)
        # [get] all the dependencies except dksdk-cmake if we already did it
        foreach(dependency IN LISTS dependencies)
            if(dependency STREQUAL "dksdk-cmake" AND _already_got_dksdk_cmake)
                continue()
            endif()
            do_get(NAME "${dependency}" INSTALL_PREFIX "${installPrefix}" CACHE_DIR "${cacheDir}")
        endforeach()
    endif()
endif()
