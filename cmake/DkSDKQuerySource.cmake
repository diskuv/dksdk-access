include_guard()
include("${CMAKE_CURRENT_LIST_DIR}/DkSDKCloneSource.cmake")

# Text that indicates the URL should be skipped. Combine something reasonably unique.
string(TIMESTAMP _DkSDKQuerySource_TS "%s.%f" UTC)
string(RANDOM LENGTH 16 _DkSDKQuerySource_INSECURERANDOM)
#   Must be usable in a regex (ie. if(MATCHES)). So UUID is perfect.
string(UUID _DkSDKQuerySource_MARKER
    NAMESPACE "9772a530-d2bb-4cef-a81f-a743f2d08bc9"
    NAME "l0@${CMAKE_CURRENT_BINARY_DIR},l1@${CMAKE_CURRENT_LIST_DIR},l2@${_DkSDKQuerySource_TS},l3@${_DkSDKQuerySource_INSECURERANDOM}"
    TYPE SHA1)
set(DkSDKQuerySource_NOT_IN_SANDBOX "not/in/sandbox/${_DkSDKQuerySource_MARKER}")
unset(_DkSDKQuerySource_TS)
unset(_DkSDKQuerySource_INSECURERANDOM)
unset(_DkSDKQuerySource_MARKER)

function(_DkSDKQuerySource_ParseJsonUrls)
    set(noValues)
    set(singleValues OUTPUT_URLS)
    set(multiValues JSON_URLS)
    cmake_parse_arguments(PARSE_ARGV 0 ARG "${noValues}" "${singleValues}" "${multiValues}")

    string(JSON len LENGTH "${ARG_JSON_URLS}")
    set(output_urls)
    if(len)
        math(EXPR len_MINUS1 "${len} - 1")
        foreach(idx RANGE ${len_MINUS1})
            # Get the raw text of the URL, like: file://${sourceParentDir}/dksdk-cmake
            string(JSON url_RAW GET "${ARG_JSON_URLS}" ${idx})
            list(APPEND output_urls "${url_RAW}")
        endforeach()
    endif()
    set(${ARG_OUTPUT_URLS} "${output_urls}" PARENT_SCOPE)
endfunction()

# Check if the [URL] exists and can be successfully authenticated.
#
# ASSUME_NETWORK: When specified, the authentication check is not performed and success is always true.
# NETWORK: When not specified, no git+https:// or git_ssh:// urls will be checked.
function(_DkSDKQuerySource_ParseAndStoreValidUrl)
    set(noValues NETWORK ASSUME_NETWORK VALIDATE)
    set(singleValues NAME URL SUCCESS_VARIABLE GIT_AUTH_RETRIES)
    set(multiValues)
    cmake_parse_arguments(PARSE_ARGV 0 ARG "${noValues}" "${singleValues}" "${multiValues}")

    # GIT_AUTH_RETRIES
    if(ARG_GIT_AUTH_RETRIES)
        set(git_auth_RETRIES "${ARG_GIT_AUTH_RETRIES}")
    else()
        set(git_auth_RETRIES 3)
    endif()

    # file://destination?mirror=source[&.git][&immutable] ?
    # Confer why we use this URL: https://www.w3.org/TR/webarch/#avoid-uri-aliases
    if(ARG_URL MATCHES "^file://(.+)[?]mirror=([^&]+)(.*)")
        # TODO: We really need URI decoding!
        # Confer: https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/decodeURI
        set(destinationPath "${CMAKE_MATCH_1}")
        set(sourcePath "${CMAKE_MATCH_2}")
        set(otherQueryParams "${CMAKE_MATCH_3}")
        set(includeDotGit OFF)
        set(immutable OFF)
        if(otherQueryParams MATCHES "&[.]git(=1)?(&|$)")
            set(includeDotGit ON)
        endif()
        if(otherQueryParams MATCHES "&immutable(=1)?(&|$)")
            set(immutable ON)
        endif()
        cmake_path(NORMAL_PATH sourcePath)
        cmake_path(NORMAL_PATH destinationPath)
        if(IS_DIRECTORY "${sourcePath}")
            if(ARG_SUCCESS_VARIABLE)
                set(${ARG_SUCCESS_VARIABLE} ON PARENT_SCOPE)
            endif()
            set_property(GLOBAL PROPERTY dependency_${ARG_NAME}                     LOCAL_MIRROR)
            set_property(GLOBAL PROPERTY dependency_${ARG_NAME}_LOCAL_MIRROR_FROM   "${sourcePath}")
            set_property(GLOBAL PROPERTY dependency_${ARG_NAME}_LOCAL_MIRROR_TO     "${destinationPath}")
            set_property(GLOBAL PROPERTY dependency_${ARG_NAME}_LOCAL_MIRROR_INCLUDE_DOTGIT "${includeDotGit}")
            set_property(GLOBAL PROPERTY dependency_${ARG_NAME}_LOCAL_MIRROR_IMMUTABLE      "${immutable}")
            return()
        elseif(IS_DIRECTORY "${destinationPath}")
            if(ARG_SUCCESS_VARIABLE)
                set(${ARG_SUCCESS_VARIABLE} ON PARENT_SCOPE)
            endif()
            set_property(GLOBAL PROPERTY dependency_${ARG_NAME}             LOCAL_DIR)
            set_property(GLOBAL PROPERTY dependency_${ARG_NAME}_LOCAL_DIR   "${destinationPath}")
            return()
        endif()
    endif()

    # file:// ?
    if(ARG_URL MATCHES "^file://(.*)")
        set(pathAfterScheme "${CMAKE_MATCH_1}")
        if(IS_DIRECTORY "${pathAfterScheme}")
            cmake_path(NORMAL_PATH pathAfterScheme OUTPUT_VARIABLE localDir)
            if(ARG_SUCCESS_VARIABLE)
                set(${ARG_SUCCESS_VARIABLE} ON PARENT_SCOPE)
            endif()
            set_property(GLOBAL PROPERTY dependency_${ARG_NAME}             LOCAL_DIR)
            set_property(GLOBAL PROPERTY dependency_${ARG_NAME}_LOCAL_DIR   "${localDir}")
            return()
        endif()
    endif()

    # https://xxx.git or https://xxx.git#some-tag or git+https:// ?
    if((ARG_ASSUME_NETWORK OR ARG_NETWORK) AND
            (ARG_URL MATCHES "^git+(https://[^#]*)($|#.*)"
            OR ARG_URL MATCHES "^(https://[^#]*[.]git)($|#.*)"))
        set(gitRepository "${CMAKE_MATCH_1}")
        set(gitRef)
        if(CMAKE_MATCH_COUNT EQUAL 2)
            set(gitRef "${CMAKE_MATCH_2}")
            string(REGEX REPLACE "^#" "" gitRef "${gitRef}")
        endif()

        set(quiet_ARGS)
        find_package(Git QUIET REQUIRED)
        foreach(tries RANGE 1 ${git_auth_RETRIES})
            if(tries LESS ${git_auth_RETRIES})
                set(quiet_ARGS ERROR_QUIET)
            endif()
            if(ARG_ASSUME_NETWORK)
                set(errorCode 0)
            else()
                execute_process(
                    COMMAND
                    ${_interactivity_PRELUDE}
                    #   We _could_ use [git ls-remote <gitref>] to check if the tag exists (or if the "head" exists), but
                    #   that is pointless complexity.
                    #   In fact, we should error out only when the [git clone ... some-tag] is being performed.
                    #   And we can't distinguish between a branch/tag or a commit id (the latter won't be in ls-remote).
                    "${GIT_EXECUTABLE}" ${_interactivity_GITOPTIONS} ls-remote --heads --quiet "${gitRepository}" _some_nonexistent_branch
                    RESULT_VARIABLE errorCode
                    ${quiet_ARGS}
                )
            endif()
            if (NOT errorCode)
                if(ARG_SUCCESS_VARIABLE)
                    set(${ARG_SUCCESS_VARIABLE} ON PARENT_SCOPE)
                endif()
                set_property(GLOBAL PROPERTY dependency_${ARG_NAME}                 GIT_HTTPS)
                set_property(GLOBAL PROPERTY dependency_${ARG_NAME}_GIT_REPOSITORY  "${gitRepository}")
                set_property(GLOBAL PROPERTY dependency_${ARG_NAME}_GIT_REF         "${gitRef}")
                return()
            endif()
        endforeach()
    endif()

    # xyz@abc.com:x/z/z.git
    if((ARG_ASSUME_NETWORK OR ARG_NETWORK) AND ARG_URL MATCHES "^([^@:]*@[^@:]*:.*[.]git)($|#.*)")
        set(gitRepository "${CMAKE_MATCH_1}")
        set(gitRef)
        if(CMAKE_MATCH_COUNT EQUAL 2)
            set(gitRef "${CMAKE_MATCH_2}")
            string(REGEX REPLACE "^#" "" gitRef "${gitRef}")
        endif()

        set(quiet_ARGS)
        find_package(Git QUIET REQUIRED)
        foreach(tries RANGE 1 ${git_auth_RETRIES})
            if(tries LESS ${git_auth_RETRIES})
                set(quiet_ARGS ERROR_QUIET)
            endif()
            if(ARG_ASSUME_NETWORK)
                set(errorCode 0)
            else()
                execute_process(
                    COMMAND
                    ${_interactivity_PRELUDE}
                    "${GIT_EXECUTABLE}" ${_interactivity_GITOPTIONS} ls-remote --heads --quiet "${gitRepository}" _some_nonexistent_branch
                    RESULT_VARIABLE errorCode
                    ${quiet_ARGS}
                )
            endif()
            if (NOT errorCode)
                if(ARG_SUCCESS_VARIABLE)
                    set(${ARG_SUCCESS_VARIABLE} ON PARENT_SCOPE)
                endif()
                set_property(GLOBAL PROPERTY dependency_${ARG_NAME}                 GIT_SSH)
                set_property(GLOBAL PROPERTY dependency_${ARG_NAME}_GIT_REPOSITORY  "${gitRepository}")
                set_property(GLOBAL PROPERTY dependency_${ARG_NAME}_GIT_REF         "${gitRef}")
                return()
            endif()
        endforeach()
    endif()

    # Not valid
    if(ARG_SUCCESS_VARIABLE)
        set(${ARG_SUCCESS_VARIABLE} OFF PARENT_SCOPE)
    endif()
endfunction()

# Find the first existing + successfully authenticated URL.
# Skips any that are outside the sandbox if SANDBOX is on.
function(_DkSDKQuerySource_GetAndStoreMacroExpandedValidUrl)
    set(noValues NETWORK)
    set(singleValues NAME SUCCESS_VARIABLE OUTPUT_URLS_VISITED GIT_AUTH_RETRIES)
    set(multiValues URLS)
    cmake_parse_arguments(PARSE_ARGV 0 ARG "${noValues}" "${singleValues}" "${multiValues}")

    # NETWORK
    set(network_EXPAND)
    if(ARG_NETWORK)
        set(network_EXPAND NETWORK)
    endif()

    # GIT_AUTH_RETRIES
    set(git_auth_retries_EXPAND)
    if(ARG_GIT_AUTH_RETRIES)
        set(git_auth_retries_EXPAND GIT_AUTH_RETRIES "${ARG_GIT_AUTH_RETRIES}")
    endif()

    set(output_urls)
    foreach(url_RAW IN LISTS ARG_URLS)
        # Set ${dependencyName}
        set(dependencyName "${ARG_NAME}")
        # Expand using CMake variables
        string(CONFIGURE "${url_RAW}" url_EXPAND)
        string(APPEND output_urls "${url_EXPAND}\n")
        message(VERBOSE "idx = ${idx} | item = ${url_RAW} | item_EXPAND = ${url_EXPAND}")
        # Check if outside the sandbox
        if(url_EXPAND MATCHES "${DkSDKQuerySource_NOT_IN_SANDBOX}")
            message(VERBOSE "skip since outside the sandbox")
            continue()
        endif()
        # Check if valid and return if so
        _DkSDKQuerySource_ParseAndStoreValidUrl(NAME "${ARG_NAME}" URL "${url_EXPAND}"
            ${network_EXPAND} ${git_auth_retries_EXPAND}
            SUCCESS_VARIABLE is_valid)
        if(is_valid)
            set(${ARG_SUCCESS_VARIABLE} "${url_EXPAND}" PARENT_SCOPE)
            set(${ARG_OUTPUT_URLS_VISITED} "${output_urls}" PARENT_SCOPE)
            return()
        endif()
    endforeach()
    set(${ARG_SUCCESS_VARIABLE} NOTFOUND PARENT_SCOPE)
    set(${ARG_OUTPUT_URLS_VISITED} "${output_urls}" PARENT_SCOPE)
endfunction()

# Compute: sourceDir and sourceParentDir and project analogs
# When IS_SANDBOX is TRUE, the value of DkSDKQuerySource_NOT_IN_SANDBOX is
# placed in the variables that represent directories outside of the sandbox.
macro(_DkSDKQuerySource_CalculateDirVariables VAR_ID IS_SANDBOX)
    cmake_path(GET ${VAR_ID}Dir PARENT_PATH ${VAR_ID}ParentDir)
    cmake_path(NATIVE_PATH ${VAR_ID}Dir ${VAR_ID}Dir_NATIVE)
    cmake_path(NATIVE_PATH ${VAR_ID}ParentDir ${VAR_ID}ParentDir_NATIVE)
    string(REPLACE "\\" "\\\\" ${VAR_ID}Dir_NATIVE_ESCAPED "${${VAR_ID}Dir_NATIVE}")
    string(REPLACE "\\" "\\\\" ${VAR_ID}ParentDir_NATIVE_ESCAPED "${${VAR_ID}ParentDir_NATIVE}")

    # Export to caller of DkSDKQuerySource_Init()
    set(${VAR_ID}Dir_NATIVE "${${VAR_ID}Dir_NATIVE}" PARENT_SCOPE)
    set(${VAR_ID}Dir_NATIVE_ESCAPED "${${VAR_ID}Dir_NATIVE_ESCAPED}" PARENT_SCOPE)
    cmake_policy(PUSH)
    if(POLICY CMP0012)
        cmake_policy(SET CMP0012 NEW) # if() recognizes numbers and boolean constants
    endif()
    if("${IS_SANDBOX}")
        set(${VAR_ID}ParentDir "${DkSDKQuerySource_NOT_IN_SANDBOX}" PARENT_SCOPE)
        set(${VAR_ID}ParentDir_NATIVE "${DkSDKQuerySource_NOT_IN_SANDBOX}" PARENT_SCOPE)
        set(${VAR_ID}ParentDir_NATIVE_ESCAPED "${DkSDKQuerySource_NOT_IN_SANDBOX}" PARENT_SCOPE)
    else()
        set(${VAR_ID}ParentDir "${${VAR_ID}ParentDir}" PARENT_SCOPE)
        set(${VAR_ID}ParentDir_NATIVE "${${VAR_ID}ParentDir_NATIVE}" PARENT_SCOPE)
        set(${VAR_ID}ParentDir_NATIVE_ESCAPED "${${VAR_ID}ParentDir_NATIVE_ESCAPED}" PARENT_SCOPE)
    endif()
    cmake_policy(POP)
endmacro()

set(_DkSDKQuerySource_dkproject_jsonc_DEFAULT [[
  {
    "dependencies": {
      // This is the default REQUEST_DKSDK_VERSION
      "dksdk": "1.0"
    }
  }
]])

# Sets REQUEST_DKSDK_VERSION and places the configuration contents of CONFIG_FILE into CONTENT_VARIABLE.
# SOURCE_DIR and PROJECT_DIR will be translated into variables like ${sourceDir} and ${projectParentDir}.
# A default dkproject.jsonc will be used if CONFIG_FILE does not exist or is not supplied.
#
# The default {dependencies: dksdk: "..."} is "1.0".
function(DkSDKQuerySource_Init)
    cmake_parse_arguments(PARSE_ARGV 0 ARG "SANDBOX" "CONFIG_FILE;CONTENT_VARIABLE;SOURCE_DIR;PROJECT_DIR" "")

    if(ARG_CONFIG_FILE AND EXISTS "${ARG_CONFIG_FILE}")
        file(READ "${ARG_CONFIG_FILE}" content)
    else()
        set(content "${_DkSDKQuerySource_dkproject_jsonc_DEFAULT}")
    endif()

    # Find: REQUEST_DKSDK_VERSION (default 1.0)
    string(JSON REQUEST_DKSDK_VERSION ERROR_VARIABLE dependencies_ERROR GET "${content}" dependencies dksdk)
    if(dependencies_ERROR)
        set(REQUEST_DKSDK_VERSION "1.0")
    endif()

    set(sourceDir "${ARG_SOURCE_DIR}")
    set(projectDir "${ARG_PROJECT_DIR}")
    _DkSDKQuerySource_CalculateDirVariables(source "${ARG_SANDBOX}")
    _DkSDKQuerySource_CalculateDirVariables(project "${ARG_SANDBOX}")

    set(sourceDir "${sourceDir}" PARENT_SCOPE)
    set(projectDir "${projectDir}" PARENT_SCOPE)
    set(REQUEST_DKSDK_VERSION "${REQUEST_DKSDK_VERSION}" PARENT_SCOPE)
    set("${ARG_CONTENT_VARIABLE}" "${content}" PARENT_SCOPE)
endfunction()

function(DkSDKQuerySource_PrepareInstallPrefixDirectory)
    cmake_parse_arguments(PARSE_ARGV 0 ARG "" "INSTALL_PREFIX" "")
    
    # DFile exclusions so that IDEs + build systems (especially Dune) don't see/scan
    # content

    #   Write .gitignore
    if(NOT EXISTS "${ARG_INSTALL_PREFIX}/.gitignore")
        file(CONFIGURE OUTPUT "${ARG_INSTALL_PREFIX}/.gitignore" CONTENT [[
# Generated by the DkSDKQuerySource module of the dksdk-access project.
#   Exclude everything except [dune] which dksdk-access generates, and
#   exclude this file [.gitignore]
*
!dune
!.gitignore
]] @ONLY NEWLINE_STYLE UNIX)
    endif()

    #   Write dune
    if(NOT EXISTS "${ARG_INSTALL_PREFIX}/dune")
        file(CONFIGURE OUTPUT "${ARG_INSTALL_PREFIX}/dune" CONTENT [[
; Generated by the DkSDKQuerySource module of the dksdk-access project.
(dirs) ; disable scanning of any subdirectories
]] @ONLY NEWLINE_STYLE UNIX)
    endif()
endfunction()

# Parse dkconfig.jsonc.
#
# Dependency code is mirrored if there are any file://?mirror= URLs.
#
# NETWORK: Don't skip over git+https:// and git+ssh:// URLs.
#
# VALIDATE: Validate that at least one URL per dependency exists and is accessible.
#
# DEFAULT_FETCH_SUBDIR: Use a different default than 'fetch'. Always best to specify it.
#
# ONLY_DKSDK_CMAKE: Only do the dksdk-cmake dependency. The dksdk-access dependency
# is always added, regardless of this flag
function(DkSDKQuerySource_ParseConfig)
    set(noValues NETWORK VALIDATE ONLY_DKSDK_CMAKE)
    set(singleValues CONTENT GIT_AUTH_RETRIES DEPENDENCIES_VARIABLE FETCH_SUBDIR_VARIABLE DEFAULT_FETCH_SUBDIR)
    set(multiValues)
    cmake_parse_arguments(PARSE_ARGV 0 ARG "${noValues}" "${singleValues}" "${multiValues}")

    # NETWORK
    set(network_EXPAND)
    if(ARG_NETWORK)
        set(network_EXPAND NETWORK)
    endif()

    # GIT_AUTH_RETRIES
    set(git_auth_retries_EXPAND)
    if(ARG_GIT_AUTH_RETRIES)
        set(git_auth_retries_EXPAND GIT_AUTH_RETRIES "${ARG_GIT_AUTH_RETRIES}")
    endif()

    # Find "fetch" subdirectory variable
    string(JSON fetchSubdir ERROR_VARIABLE _ignore GET "${ARG_CONTENT}" "fetchSubdirectory")
    if(fetchSubdir)
        cmake_path(NORMAL_PATH fetchSubdir)
        cmake_path(IS_RELATIVE fetchSubdir is_fetch_relative)
        if(NOT is_fetch_relative)
            message(FATAL_ERROR "The [fetchSubdirectory] must be defined as a relative path in dkproject.jsonc")
        endif()
        if(fetchSubdir MATCHES "[.][.]") # No ../ allowed.
            message(FATAL_ERROR "The [fetchSubdirectory] must be defined as a subdirectory in dkproject.jsonc")
        endif()
    elseif(ARG_DEFAULT_FETCH_SUBDIR)
        set(fetchSubdir "${ARG_DEFAULT_FETCH_SUBDIR}")
    else()
        set(fetchSubdir "fetch") # deprecated!
    endif()
    if(ARG_FETCH_SUBDIR_VARIABLE)
        set("${ARG_FETCH_SUBDIR_VARIABLE}" "${fetchSubdir}" PARENT_SCOPE)
    endif()

    # Parsing loop for explicit dependencies
    string(JSON _dependencies_LENGTH ERROR_VARIABLE _dependencies_ERROR LENGTH "${ARG_CONTENT}" dependencies)
    set(dependencies)
    if(_dependencies_LENGTH)
        math(EXPR _dependencies_LENGTH_MINUS1 "${_dependencies_LENGTH} - 1")
        foreach(idx RANGE ${_dependencies_LENGTH_MINUS1})
            # Find name of dependency
            string(JSON dependency MEMBER "${ARG_CONTENT}" dependencies ${idx})

            # We've already processed the [dksdk] dependency
            if(dependency STREQUAL dksdk)
                continue()
            endif()

            # Filter if ONLY_DKSDK_CMAKE
            if(ARG_ONLY_DKSDK_CMAKE AND NOT dependency STREQUAL dksdk-cmake)
                continue()
            endif()

            # Get type and value of dependency
            string(JSON type TYPE "${ARG_CONTENT}" dependencies "${dependency}")
            string(JSON value GET "${ARG_CONTENT}" dependencies "${dependency}")
            message(VERBOSE "dependency = ${dependency} | type = ${type} | value = ${value}")

            if(type STREQUAL OBJECT)
                string(JSON abstract ERROR_VARIABLE _ignore GET "${ARG_CONTENT}" dependencies "${dependency}" abstract)
                string(JSON inherits ERROR_VARIABLE _ignore GET "${ARG_CONTENT}" dependencies "${dependency}" inherits)
                string(JSON urls GET "${ARG_CONTENT}" dependencies "${dependency}" urls)

                # Parse into raw [urls]
                _DkSDKQuerySource_ParseJsonUrls(JSON_URLS "${urls}" OUTPUT_URLS urls_RAW)

                # Prepend to [urls_RAW] if there is any inheritance
                if(inherits)
                    get_property(inherits_URLS GLOBAL PROPERTY "dependency_${inherits}_URLS")
                    list(PREPEND urls_RAW ${inherits_URLS})
                endif()

                # Store the [urls] including any inheritance
                set_property(GLOBAL APPEND PROPERTY "dependency_${dependency}_URLS" ${urls_RAW})

                # Macro expand and store any concrete (non-abstract) dependency
                if(NOT abstract)
                    _DkSDKQuerySource_GetAndStoreMacroExpandedValidUrl(NAME "${dependency}" URLS ${urls_RAW}
                        ${network_EXPAND}
                        SUCCESS_VARIABLE validUrl OUTPUT_URLS_VISITED visitedUrls)
                    if(ARG_VALIDATE AND NOT validUrl)
                        message(FATAL_ERROR "Could not get a valid URL for dependency `${dependency}` from among the following URLs: ${urls}\n[TRACE] The URLs seen so far are: ${visitedUrls}")
                    endif()
                    if(validUrl)
                        list(APPEND dependencies "${dependency}")
                    endif()
                endif()
            else()
                message(FATAL_ERROR "Unsupported dependency `${dependency}: ${value}`. Only JSON objects are supported.")
            endif()
        endforeach()
    endif()

    # Add the implicit [dksdk-access] dependency
    if(NOT dksdk-access IN_LIST dependencies)
        # <dksdk-acess>/cmake/scripts --> <dksdk-access>
        cmake_path(GET CMAKE_CURRENT_LIST_DIR PARENT_PATH localDir)
        cmake_path(GET localDir PARENT_PATH localDir)
        set_property(GLOBAL PROPERTY dependency_dksdk-access            LOCAL_DIR)
        set_property(GLOBAL PROPERTY dependency_dksdk-access_LOCAL_DIR  "${localDir}")
        set_property(GLOBAL PROPERTY dependency_dksdk-access_URLS       "file://${localDir}")
        list(PREPEND dependencies dksdk-access) # Want this dependency up front for consistency of position.
    endif()

    if(ARG_DEPENDENCIES_VARIABLE)
        set(${ARG_DEPENDENCIES_VARIABLE} ${dependencies} PARENT_SCOPE)
    endif()
endfunction()

# DkSDKQuerySource_FindSource() will produce CMake FetchDeclare() options for the
# dependency named <NAME>.
#
# When the source has a mirror, the function will immediately perform mirroring
# and give back a FetchDeclare(URL) option that to the mirror destination directory.
function(DkSDKQuerySource_FindSource)
    set(noValues)
    set(singleValues CONTENT NAME DEFAULT_FETCH_SUBDIR
        DOWNLOAD_OPTIONS_VARIABLE SUCCESS_VARIABLE FETCH_SUBDIR_VARIABLE)
    set(multiValues)
    cmake_parse_arguments(PARSE_ARGV 0 ARG "${noValues}" "${singleValues}" "${multiValues}")

    # Parse all the local (non-network) URLs
    set(parseConfig_ARGS)
    if(ARG_FETCH_SUBDIR_VARIABLE)
        list(APPEND parseConfig_ARGS FETCH_SUBDIR_VARIABLE fetchSubdir)
    endif()
    if(ARG_DEFAULT_FETCH_SUBDIR)
        list(APPEND parseConfig_ARGS DEFAULT_FETCH_SUBDIR "${ARG_DEFAULT_FETCH_SUBDIR}")
    endif()
    DkSDKQuerySource_ParseConfig(
        # No [NETWORK]
        CONTENT "${ARG_CONTENT}"
        ${parseConfig_ARGS}
    )
    if(ARG_FETCH_SUBDIR_VARIABLE)
        set("${ARG_FETCH_SUBDIR_VARIABLE}" "${fetchSubdir}" PARENT_SCOPE)
    endif()

    get_property(local_mirror_from GLOBAL PROPERTY dependency_${ARG_NAME}_LOCAL_MIRROR_FROM)
    get_property(local_mirror_to GLOBAL PROPERTY dependency_${ARG_NAME}_LOCAL_MIRROR_TO)
    get_property(local_mirror_include_dotgit GLOBAL PROPERTY dependency_${ARG_NAME}_LOCAL_MIRROR_INCLUDE_DOTGIT)
    get_property(local_mirror_immutable GLOBAL PROPERTY dependency_${ARG_NAME}_LOCAL_MIRROR_IMMUTABLE)
    get_property(local_dir GLOBAL PROPERTY dependency_${ARG_NAME}_LOCAL_DIR)
    get_property(urls GLOBAL PROPERTY dependency_${ARG_NAME}_URLS)

    # Did the source exist?
    if(NOT urls)
        set("${ARG_SUCCESS_VARIABLE}" OFF PARENT_SCOPE)
        return()
    endif()
    list(LENGTH urls urls_LEN)

    if(local_dir)
        if(ARG_DOWNLOAD_OPTIONS_VARIABLE)
            set("${ARG_DOWNLOAD_OPTIONS_VARIABLE}"
                URL "${local_dir}"
                PARENT_SCOPE)
        endif()
        set("${ARG_SUCCESS_VARIABLE}" ON PARENT_SCOPE)
        return()
    elseif(local_mirror_to)
        # Immediately do mirroring
        set(clone_ARGS)
        if(local_mirror_include_dotgit)
            list(APPEND clone_ARGS INCLUDE_DOTGIT)
        endif()
        if(local_mirror_immutable)
            list(APPEND clone_ARGS IMMUTABLE)
        endif()
        DkSDKCloneSource_CloneLocalDir(
            SOURCE_DIR "${local_mirror_from}"
            DESTINATION_DIR "${local_mirror_to}"
            OUTPUT_DESTINATION_VARIABLE local_DESTINATION
            ${clone_ARGS}
        )
        if(ARG_DOWNLOAD_OPTIONS_VARIABLE)
            set("${ARG_DOWNLOAD_OPTIONS_VARIABLE}"
                URL "${local_DESTINATION}"
                PARENT_SCOPE)
        endif()
        set("${ARG_SUCCESS_VARIABLE}" ON PARENT_SCOPE)
        return()
    else()
        # Otherwise use the last URL
        list(GET urls -1 lastUrl_RAW)
        #   Expand using CMake variables
        string(CONFIGURE "${lastUrl_RAW}" lastUrl_EXPAND)
        message(VERBOSE "DkSDKQuerySource_FindSource(lastUrl_RAW=${lastUrl_RAW} lastUrl_EXPAND=${lastUrl_EXPAND})")

        _DkSDKQuerySource_ParseAndStoreValidUrl(
            NAME "${ARG_NAME}"
            URL "${lastUrl_EXPAND}"
            ASSUME_NETWORK)

        # Only non-dir URL is a Git URL (today!)
        get_property(git_repository GLOBAL PROPERTY dependency_${ARG_NAME}_GIT_REPOSITORY)
        get_property(git_ref GLOBAL PROPERTY dependency_${ARG_NAME}_GIT_REF)

        if(NOT git_repository)
            message(FATAL_ERROR "Unsupported URL in the dkproject.jsonc configuration: ${lastUrl_RAW}")
        endif()
        set(git_tag_EXPAND)
        if(git_ref)
            list(APPEND git_tag_EXPAND GIT_TAG "${git_ref}")
        else()
            list(APPEND git_tag_EXPAND GIT_TAG "origin/main")
        endif()
        if(ARG_DOWNLOAD_OPTIONS_VARIABLE)
            set("${ARG_DOWNLOAD_OPTIONS_VARIABLE}"
                GIT_REPOSITORY "${git_repository}"
                ${git_tag_EXPAND}
                PARENT_SCOPE)
        endif()
        set("${ARG_SUCCESS_VARIABLE}" ON PARENT_SCOPE)
        return()
    endif()

    set("${ARG_SUCCESS_VARIABLE}" OFF PARENT_SCOPE)
endfunction()
