include_guard()

function(_DkSDKDetectSourceChange_Checksum_dir DIR VARIABLE_CKSUM)
     # cmake manual> The results will be ordered lexicographically.
     file(GLOB_RECURSE listing FOLLOW_SYMLINKS LIST_DIRECTORIES false "${DIR}/*")
     # Remove: .cxx, .git and .gradle directories
     list(FILTER listing EXCLUDE REGEX "/(\\.cxx|\\.git|\\.gradle)/")

     set(acc)
     foreach(file IN LISTS listing)
          # MD5 is for developer productivity change detection, not security.
          # Needs to be fast.
          file(MD5 "${file}" cksum)
          list(APPEND acc "${cksum}")
     endforeach()

     # calculate final checksum and return
     string(CONCAT final_cksum ${acc})
     string(MD5 final_cksum "${final_cksum}")
     set(${VARIABLE_CKSUM} "${final_cksum}" PARENT_SCOPE)
endfunction()

function(DkSDKDetectSourceChange_Checksum ENTRY VARIABLE_CKSUM)
     if(IS_DIRECTORY "${ENTRY}")
          _DkSDKDetectSourceChange_Checksum_dir("${ENTRY}" cksum)
     else()
          file(MD5 "${ENTRY}" cksum)
     endif()
     set(${VARIABLE_CKSUM} "${cksum}" PARENT_SCOPE)
endfunction()
