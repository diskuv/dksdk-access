# Specify the assembly programming language.
if(ANDROID AND(CMAKE_ANDROID_ARCH_ABI STREQUAL x86 OR CMAKE_ANDROID_ARCH_ABI STREQUAL x86_64))
    # Netwide assembler.
    # Android x86/x86_64 per https://developer.android.com/ndk/guides/cmake#yasm-cmake
    set(asmlang ASM_NASM)
elseif(WIN32)
    # Microsoft assembly language, only for Windows
    set(asmlang ASM_MASM)
else()
    # Generic assembly language. Typically the assembly language built into the C compiler.
    set(asmlang ASM)
endif()
