# DkSDK Access

- [DkSDK Access](#dksdk-access)
  - [Using in your CMake project](#using-in-your-cmake-project)
    - [Importing DkSDK CMake](#importing-dksdk-cmake)
    - [Importing other DkSDK projects](#importing-other-dksdk-projects)
  - [CMake Variables](#cmake-variables)
    - [FETCHCONTENT\_SOURCE\_DIR\_DKSDK-ACCESS](#fetchcontent_source_dir_dksdk-access)
    - [FETCHCONTENT\_SOURCE\_DIR\_DKSDK-CMAKE](#fetchcontent_source_dir_dksdk-cmake)
  - [dkproject.jsonc](#dkprojectjsonc)
    - [Development variants of dkproject.jsonc](#development-variants-of-dkprojectjsonc)
  - [Environment Variables](#environment-variables)
    - [DKSDK\_ACCESS\_GIT\_AUTH\_RETRIES](#dksdk_access_git_auth_retries)
    - [Dependencies](#dependencies)
      - [Local directories `file://`](#local-directories-file)
      - [HTTPS `https://` and SSH `username@git.org/`](#https-https-and-ssh-usernamegitorg)

## Using in your CMake project

### Importing DkSDK CMake

To import DkSDK CMake into your CMake project, use:

```cmake
# Preamble
cmake_minimum_required(VERSION 3.22)

#   Import the DkSDK framework
include(FetchContent)
set(REQUEST_DKSDK_VERSION 1.0)
FetchContent_Declare(dksdk-access
    GIT_REPOSITORY https://gitlab.com/diskuv/dksdk-access.git
    GIT_TAG origin/main)
FetchContent_MakeAvailable(dksdk-access)
get_property(dksdk-cmake_MODULES GLOBAL PROPERTY DKSDK_CMAKE_MODULES)
get_property(dksdk-cmake_LANG_ASM GLOBAL PROPERTY DKSDK_CMAKE_LANG_ASM)
list(APPEND CMAKE_MODULE_PATH ${dksdk-cmake_MODULES})

#   Define the project
project(YourProjectName VERSION 1.0.0 LANGUAGES C CXX OCamlDune ${dksdk-cmake_LANG_ASM})
```

Then use the [DkSDK CMake Subscriber Access Guide](https://diskuv.com/cmake/help/latest/guide/subscriber-access/) to configure the environment.

### Importing other DkSDK projects

One you have imported DkSDK CMake, you can import other DkSDK projects in your `dependencies/CMakeLists.txt`:

```cmake
# This section should be at the top of your dependencies/CMakeLists.txt
include(DkSDKProject)
include(DkSDKFetchContent)
DkSDKProject_AddDependencies()

# For DkSDK FFI OCaml only
include(DkSDKRequire)
DkSDKRequire_Add(FFI OCaml)

# For DkSDK FFI C
get_property(dksdk-ffi-c_REPOSITORY GLOBAL PROPERTY DKSDK_FFI_C_REPOSITORY)
DkSDKFetchContent_DeclareSecondParty(NAME dksdk-ffi-c
    GIT_REPOSITORY "${dksdk-ffi-c_REPOSITORY}"
    GIT_TAG origin/main)
DkSDKFetchContent_MakeAvailableNoInstall(dksdk-ffi-c)

# For all the other DkSDK projects, use DkSDK FFI C as the example.
# Future versions of DkSDK may allow you to use DkSDKRequire_Add() instead.
```

## CMake Variables

### FETCHCONTENT_SOURCE_DIR_DKSDK-ACCESS

This variable can be set to the existing directory path of the `dksdk-access` project.

### FETCHCONTENT_SOURCE_DIR_DKSDK-CMAKE

This variable can be set to the existing directory path of the `dksdk-cmake` project.

## dkproject.jsonc

`dkproject.jsonc` is the project configuration file. A sample one is:

```jsonc
// This is a "JSON with Comments" file popularized by Visual Studio Code [1].
// So this file is regular JSON interspersed with C-style "//" comment lines
// and "/* ... */" comment blocks.
{
  "dependencies": {
    // This dependency is required, and in this version of DkSDK it must be 1.0.
    // It provides many variables like `${dksdk-cmake_REPOSITORY}`.
    // Run `./dk dksdk.project.inspect VARIABLES` to see what is available.
    "dksdk": "1.0",
    // Abstract dependencies (ie. templates) to minimize boilerplate. They are merged
    // when you inherit from them.
    "_local": {
      "abstract": true,
      // The URLs are searched in order. The first URL that exists and can be
      // successfully authenticated is used. So any dependency that uses `_local`
      // will *first* check if the dependency is already checked out beside this project
      // or in the fetch/ subfolder of this project.
      "urls": [
        // SAFETY FEATURE: Any URLs containing sourceParentDir or
        // projectParentDir are skipped in your main dkproject.jsonc [2]
        "file://${sourceParentDir}/${dependencyName}",
        "file://${sourceDir}/fetch/${dependencyName}"
      ]
    },
    "dksdk-cmake": {
      "inherits": "_local",
      "urls": [ "${dksdk-cmake_REPOSITORY}" ]
    },
    "dkml-runtime-common": {
      "inherits": "_local",
      "urls": [ "https://github.com/diskuv/dkml-runtime-common.git" ]
    },
  }
}

// [1]: https://code.visualstudio.com/docs/languages/json#_json-with-comments
// [2]: Some CI providers (ex. GitLab CI with the "shell" runner) will only
//      clean sibling directories when a CI job is run in that sibling
//      directory. If sourceParentDir or projectParentDir were used, you would
//      risk that future CI jobs see the content of other CI jobs. You _can_
//      use a "Development variant" (below) to enable sourceParentDir or
//      projectParentDir.
```

The `dksdk-access` dependency is implied; you should not need to add it.

### Development variants of dkproject.jsonc

The first line of the file `.z-dk/project-variant`, if it exists, is the
location of an alternative `dkproject.jsonc`. This is the file you can
manipulate to load a development variant. For example, you may want to
have a project configuration that uses `sourceParentDir` or
`projectParentDir` to access sibling projects.

You should instruct your source repository to ignore the
`.z-dk/project-variant` file. Typically that is by adding the line
`.z-dk/project-variant` to your `.gitignore`.

## Environment Variables

### DKSDK_ACCESS_GIT_AUTH_RETRIES

The number of attempts to try to authenticate git with HTTPS or SSH. Defaults to `3`. May be between `1` and `5`.

In a typical git setup the git authentication only needs to be tried once. However, there are rare occasions when
multiple attempts for authentication are needed. One example is when you have an SSH agent (ex. Yubico SSH agent or
1Password SSH agent) together with a Git credentials helper (ex. GitHub Credentials Manager); your Git client may
round-robin the methods until it finds success with either the SSH agent or the credentials helper.

As a comparison, CMake uses `5` retries for downloading (which includes git authentication) in its `ExternalProject`
module.

### Dependencies

#### Local directories `file://`

Other local projects can be referenced with a URL like `file://${sourceParentDir}/projectName`.
If and when the local project's source code needs to be copied (into a build directory, through a CMake WSL2 facade, etc.) the following is checked *in order*:

1. Does the `<other-local-project>/cmake/run/CloneSource.cmake` script exist? Then that script is called from the `<other-local-project>` directory using the command `cmake -D CMAKE_INSTALL_PREFIX=<destination> -P cmake/run/CloneSource.cmake`. Done!
2. Does the `<other-local-project>/.git` directory exist? Then *every* file and directory in `<other-local-project>` is copied *except* what is in the `<other-local-project>/**/.gitignore` configuration files. Done! *This can take an insignificant time because it involves running `git -C <other-local-project> ls-files --exclude-standard -oi --directory`.*
3. The [cmake/run/CloneSource.cmake](cmake/run/CloneSource.cmake) default script from `dksdk-access` is called from the `<other-local-project>` directory using the command `cmake -D CMAKE_INSTALL_PREFIX=<destination> -P <dksdk-access>/cmake/run/CloneSource.cmake`. Done!

#### HTTPS `https://` and SSH `username@git.org/`

You can include a branch name, tag name or a git commit id (aka. a "git ref") using the syntax `username@git.org/somerepo/somepath.git#<gitref>`.

Prefer the git commit id for security, reproducibility and speed. If you need to use a branch name or a tag name, include the remote name (ex. `#origin/main` rather than `#main`) so that edge cases like git rebases and git history rewrites are handled. The default git commit id is `#main`.
