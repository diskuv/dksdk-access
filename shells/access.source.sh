#!/bin/sh

# This script is meant to be sourced. Example:
#
#    #!/bin/sh
#    set -euf
#
#    [ -e dksdk-access/shells/access.source.sh ] || git clone https://gitlab.com/diskuv/dksdk-access.git
#    . dksdk-access/shells/access.source.sh
#    dksdk_access 1.0
#
# It should strictly mimic the logic of the
# **authoritative** `cmake/DkSDKAccess.cmake``:
#
# 1. If an environment variable is not part of the logic, then
#    it won't be part of the logic for this script.
#    For example, DKSDK_DATA_HOME is a CMake variable that is checked
#    in `cmake/DkSDKAccess.cmake`. That means in this script we do
#    not look for DKSDK_DATA_HOME as an environment variable; instead
#    we assume DKSDK_DATA_HOME has not been set.
#

# Command: [dksdk_access REQUEST_DKSDK_VERSION]
#
#   Exports the following variables:
#       dksdk_access_DKSDK_DATA_HOME
#       dksdk_access_REQUEST_DKSDK_VERSION_MAJOR
#       dksdk_access_REQUEST_DKSDK_VERSION_MINOR
#       _dksdk_BASE_REPOSITORY_URL
#       _dksdk_cmake_REPOSITORY
#       _dksdk_ffi_c_REPOSITORY
#       _dksdk_ffi_ocaml_REPOSITORY
#       _dksdk_ffi_java_REPOSITORY
#       _dksdk_opam_repository_core_REPOSITORY
#       _dksdk_opam_repository_js_REPOSITORY
#       _dksdk_port_ocaml_ctypes_REPOSITORY
dksdk_access() {
    if [ $# -eq 0 ]; then
        printf "usage: dksdk_access REQUEST_DKSDK_VERSION\n" 2>&1
        return 21
    fi

    dksdk_access_REQUEST_DKSDK_VERSION=$1
    shift

    # --- Parse REQUEST_DKSDK_VERSION
    if ! printf "%s" "$dksdk_access_REQUEST_DKSDK_VERSION" | grep -q -E '^[0-9]+[.][0-9]+$'; then
        printf "The REQUEST_DKSDK_VERSION is not a valid version\n" 2>&1
        return 22
    fi

    dksdk_access_REQUEST_DKSDK_VERSION_MAJOR=$(printf "%s" "$dksdk_access_REQUEST_DKSDK_VERSION" | sed 's#[.].*##')
    dksdk_access_REQUEST_DKSDK_VERSION_MINOR=$(printf "%s" "$dksdk_access_REQUEST_DKSDK_VERSION" | sed 's#.*[.]##')
    dksdk_access_REQUEST_DKSDK_VERSION_underscore="${dksdk_access_REQUEST_DKSDK_VERSION_MAJOR}_${dksdk_access_REQUEST_DKSDK_VERSION_MINOR}"

    # --- Locate DKSDK_DATA_HOME
    if [ -n "${XDG_DATA_HOME:-}" ]
    then # ie. modern Unix or Windows CI
        dksdk_access_DKSDK_DATA_HOME="${XDG_DATA_HOME}/dksdk"
        printf "Using <XDG_DATA_HOME>/dksdk as DKSDK_DATA_HOME: %s\n" "$dksdk_access_DKSDK_DATA_HOME" 2>&1
    elif [ -x /usr/bin/cygpath ] && [ -n "${LOCALAPPDATA:-}" ]
    then # ie. Windows
        dksdk_access_DKSDK_DATA_HOME=$LOCALAPPDATA/DkSDK
        printf "Using <LOCALAPPDATA>/DkSDK as DKSDK_DATA_HOME: %s\n" "$dksdk_access_DKSDK_DATA_HOME" 2>&1
    elif [ -n "${HOME:-}" ]
    then # ie. legacy Unix
        dksdk_access_DKSDK_DATA_HOME="${HOME}/.local/share/dksdk"
        printf "Using <HOME>/.local/share/dksdk as DKSDK_DATA_HOME: %s\n" "$dksdk_access_DKSDK_DATA_HOME" 2>&1
    fi

    # --- Set dksdk-BASE_REPOSITORY_URL
    if [ -e "${dksdk_access_DKSDK_DATA_HOME}/repository.ini" ]; then
        # potential sensitive user data: Use _dksdk_access variable so not part of [set | grep ^dksdk_access_] in tests

        # override repository with file contents (prepend comment lines with a # symbol)
        _dksdk_access_repository_line=$(
            head -c 8192 "${dksdk_access_DKSDK_DATA_HOME}/repository.ini" |
            grep -E "^${dksdk_access_REQUEST_DKSDK_VERSION_underscore}"'[ \t]*=[ \t]*(https://|git@)' |
        head -n1)

        # 1_0 = https://x/y/z.git ==> https://x/y/z.git
        _dksdk_BASE_REPOSITORY_URL=$(
            printf "%s" "$_dksdk_access_repository_line" |
        sed "s#^${dksdk_access_REQUEST_DKSDK_VERSION_underscore}"'[ \t]*=[ \t]*##')

        if [ -n "$_dksdk_BASE_REPOSITORY_URL" ]; then
            printf "Using DkSDK repositories based on the %s = <redacted> repository setting in %s/repository.ini\n" "${dksdk_access_REQUEST_DKSDK_VERSION_underscore}" "${dksdk_access_DKSDK_DATA_HOME}" 2>&1
        else
            printf "WARNING: There was no %s = <redacted> repository setting in %s/repository.ini. Trying fallbacks ...\n" "${dksdk_access_REQUEST_DKSDK_VERSION_underscore}" "${dksdk_access_DKSDK_DATA_HOME}" 2>&1
        fi
    fi
    if [ -z "${_dksdk_BASE_REPOSITORY_URL:-}" ]; then # default
        # potential sensitive user data: Use _dksdk_access variable so not part of [set | grep ^dksdk_access_] in tests

        dksdk_access_VARNAME_DKSDK_BASE_REPO=DKSDK_BASE_REPO_${dksdk_access_REQUEST_DKSDK_VERSION_underscore}
        eval "_dksdk_access_DKSDK_BASE_REPO=\${${dksdk_access_VARNAME_DKSDK_BASE_REPO}:-}"
        if [ -n "$_dksdk_access_DKSDK_BASE_REPO" ]; then
            # override base of repository with environment variable
            _dksdk_BASE_REPOSITORY_URL="$_dksdk_access_DKSDK_BASE_REPO"
            printf "Using DkSDK repositories based on the %s environment variable\n" "$dksdk_access_VARNAME_DKSDK_BASE_REPO" 2>&1
        else
            _dksdk_BASE_REPOSITORY_URL="https://gitlab.com/diskuv/distributions/${dksdk_access_REQUEST_DKSDK_VERSION}"
            printf "Using default %s DkSDK repositories\n" "$_dksdk_BASE_REPOSITORY_URL" 2>&1
            printf "WARNING: It does not look like you have a valid DkSDK environment. Follow the DkSDK CMake Subscriber Access Guide at https://diskuv.com/cmake/help/latest/guide/subscriber-access/\n" 2>&1
        fi
    fi

    # --- Set dksdk-cmake_REPOSITORY
    dksdk_access_config0() {
        dksdk_access_config0_SUBPATH=$1; shift
        dksdk_access_config0_NAME_LOWER_UNDERSCORE=$1; shift
        dksdk_access_config0_NAME_UPPER_UNDERSCORE=$1; shift
        dksdk_access_config0_DESCR=$1; shift
        # Example:
        # dksdk_access_VARNAME_DKSDK_CMAKE_REPO=DKSDK_CMAKE_REPO_${dksdk_access_REQUEST_DKSDK_VERSION_underscore}
        # eval "_dksdk_access_DKSDK_CMAKE_REPO=\${${dksdk_access_VARNAME_DKSDK_CMAKE_REPO}:-}"
        # if [ -n "$_dksdk_access_DKSDK_CMAKE_REPO" ]; then
        #     _dksdk_cmake_REPOSITORY="$_dksdk_access_DKSDK_CMAKE_REPO"
        #     printf "Using DkSDK CMake repository based on the %s environment variable\n" "$dksdk_access_VARNAME_DKSDK_CMAKE_REPO" 2>&1
        # else
        #     _dksdk_cmake_REPOSITORY="${_dksdk_BASE_REPOSITORY_URL}/dksdk-cmake.git"
        # fi

        dksdk_access_config0_VARNAME=${dksdk_access_config0_NAME_UPPER_UNDERSCORE}_REPO_${dksdk_access_REQUEST_DKSDK_VERSION_underscore}
        eval "_dksdk_access_config0_REPO=\${${dksdk_access_config0_VARNAME}:-}"
        if [ -n "$_dksdk_access_config0_REPO" ]; then
            # potential sensitive user data: Use _dksdk_access variable so not part of [set | grep ^dksdk_access_] in tests

            # override repository with environment variable
            _dksdk_access_config0_FOUND_REPO="$_dksdk_access_config0_REPO"
            printf "Using %s repository based on the %s environment variable\n" "$dksdk_access_config0_DESCR" "$dksdk_access_config0_VARNAME" 2>&1
        else
            _dksdk_access_config0_FOUND_REPO="${_dksdk_BASE_REPOSITORY_URL}/${dksdk_access_config0_SUBPATH}.git"
        fi
        eval "_${dksdk_access_config0_NAME_LOWER_UNDERSCORE}_REPOSITORY='${_dksdk_access_config0_FOUND_REPO}'"
    }
    dksdk_access_config() {
        dksdk_access_config0 "$@"
    }
    dksdk_access_config_port() {
        dksdk_access_config_NAME=$1; shift
        dksdk_access_config_NAME_LOWER_UNDERSCORE=$1; shift
        dksdk_access_config_NAME_UPPER_UNDERSCORE=$1; shift
        dksdk_access_config0 "ports/$dksdk_access_config_NAME" "dksdk_port_$dksdk_access_config_NAME_LOWER_UNDERSCORE" "DKSDK_PORT_$dksdk_access_config_NAME_UPPER_UNDERSCORE" "$@"
    }
    dksdk_access_config dksdk-cmake        dksdk_cmake      DKSDK_CMAKE        "DkSDK CMake"
    dksdk_access_config dksdk-coder        dksdk_coder      DKSDK_CODER        "DkSDK Coder"
    dksdk_access_config dksdk-ffi-c        dksdk_ffi_c      DKSDK_FFI_C        "DkSDK FFI C"
    dksdk_access_config dksdk-ffi-java     dksdk_ffi_java   DKSDK_FFI_JAVA     "DkSDK FFI Java"
    dksdk_access_config dksdk-ffi-ocaml    dksdk_ffi_ocaml  DKSDK_FFI_OCAML    "DkSDK FFI OCaml"
    dksdk_access_config dksdk-opam-repository-core  dksdk_opam_repository_core  DKSDK_OPAM_REPOSITORY_CORE  "DkSDK Opam Core Repository"
    dksdk_access_config dksdk-opam-repository-js    dksdk_opam_repository_js    DKSDK_OPAM_REPOSITORY_JS    "DkSDK Opam JavaScript Repository"
    dksdk_access_config_port ocaml-ctypes     ocaml_ctypes     OCAML_CTYPES     "DkSDK port of ocaml-ctypes"

    # --- Add token
    dksdk_access_VARNAME_DKSDK_TOKEN=DKSDK_TOKEN_${dksdk_access_REQUEST_DKSDK_VERSION_underscore}
    eval "_dksdk_access_DKSDK_TOKEN=\${${dksdk_access_VARNAME_DKSDK_TOKEN}:-}"
    if [ -n "$_dksdk_access_DKSDK_TOKEN" ]; then
        _dksdk_access_CREDS=dksdk-access:$_dksdk_access_DKSDK_TOKEN
    elif [ -n "${CI_JOB_TOKEN:-}" ] && [ "${GITLAB_CI:-}" = "true" ]; then
        _dksdk_access_CREDS=gitlab-ci-token:$CI_JOB_TOKEN
    else
        _dksdk_access_CREDS=
    fi
    if [ -n "$_dksdk_access_CREDS" ]; then
        # potential sensitive user data: Use _dksdk_access variable so not part of [set | grep ^dksdk_access_] in tests

        # Group or project access token, not OAuth2 token, so do not use "oauth2" as the username.
        # Instead use "gitlab-ci-token".
        _dksdk_BASE_REPOSITORY_URL=$(printf "%s" "$_dksdk_BASE_REPOSITORY_URL" | sed -E "s#^https://#https://$_dksdk_access_CREDS@#")
        _dksdk_cmake_REPOSITORY=$(printf "%s" "$_dksdk_cmake_REPOSITORY" | sed -E "s#^https://#https://$_dksdk_access_CREDS@#")
        _dksdk_ffi_c_REPOSITORY=$(printf "%s" "$_dksdk_ffi_c_REPOSITORY" | sed -E "s#^https://#https://$_dksdk_access_CREDS@#")
        _dksdk_ffi_ocaml_REPOSITORY=$(printf "%s" "$_dksdk_ffi_ocaml_REPOSITORY" | sed -E "s#^https://#https://$_dksdk_access_CREDS@#")
        _dksdk_ffi_java_REPOSITORY=$(printf "%s" "$_dksdk_ffi_java_REPOSITORY" | sed -E "s#^https://#https://$_dksdk_access_CREDS@#")
        _dksdk_opam_repository_core_REPOSITORY=$(printf "%s" "$_dksdk_opam_repository_core_REPOSITORY" | sed -E "s#^https://#https://$_dksdk_access_CREDS@#")
        _dksdk_opam_repository_js_REPOSITORY=$(printf "%s" "$_dksdk_opam_repository_js_REPOSITORY" | sed -E "s#^https://#https://$_dksdk_access_CREDS@#")
        _dksdk_port_ocaml_ctypes_REPOSITORY=$(printf "%s" "$_dksdk_port_ocaml_ctypes_REPOSITORY" | sed -E "s#^https://#https://$_dksdk_access_CREDS@#")
        printf "Adding token from %s environment variable to the DkSDK repositories\n" "$dksdk_access_VARNAME_DKSDK_TOKEN" 2>&1
    fi
    _dksdk_access_CREDS= # unset

    # Done.
    export dksdk_access_DKSDK_DATA_HOME dksdk_access_REQUEST_DKSDK_VERSION_MAJOR dksdk_access_REQUEST_DKSDK_VERSION_MINOR
    export _dksdk_BASE_REPOSITORY_URL
    export _dksdk_cmake_REPOSITORY _dksdk_ffi_c_REPOSITORY _dksdk_ffi_ocaml_REPOSITORY _dksdk_ffi_java_REPOSITORY
    export _dksdk_opam_repository_core_REPOSITORY _dksdk_opam_repository_js_REPOSITORY
    export _dksdk_port_ocaml_ctypes_REPOSITORY
}